﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tema1
{
    /// <summary>
    /// Interaction logic for Quiz.xaml
    /// </summary>
    public partial class Quiz : Window
    {
        public Quiz()
        {
            InitializeComponent();
            AparitieImagine();
          //  metoda();
        }
        public int contor = 0;
        public int raspunsuriCorecte = 0;

        public int RaspunsuriCorecte
        {
            set
            {
                raspunsuriCorecte = value;
            }
            get
            {
                return raspunsuriCorecte;
            }
        }
        public int Contor
        {
            set
            {
                contor = value;
            }
            get
            {
                return contor;
            }
        }
        public void metoda()
        {
            Random rnd = new Random();
            List<string> lines = new List<string>();
            string[] vector = System.IO.File.ReadAllLines(@"C:\Users\Radina\Desktop\ANUL 2\MVP\Tema1\text.txt");
            for (int i = 0; i < vector.Length - 3; i += 4)
            {
                lines.Add(vector[i + 2]);
            }
            int index = rnd.Next(lines.Count());
            intrebare.Text = lines[index];
        }
        private void AparitieImagine()
        {
            Random rnd = new Random();
            List<string> ListaImagini = new List<string>();
          string[] imagini = System.IO.File.ReadAllLines(@"C:\Users\Radina\Desktop\ANUL 2\MVP\Tema1\text.txt");
          for (int indexx = 0; indexx < imagini.Length - 3; indexx += 4)
          {
               ListaImagini.Add(imagini[indexx + 3]);
            }
            int index = rnd.Next(ListaImagini.Count());

           image.Source = new BitmapImage(new Uri(ListaImagini[index]));
         
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string cuvant, descriere;
            cuvant = raspuns.Text;
            descriere = intrebare.Text;
            List<string> lines = new List<string>();
            string[] vector = System.IO.File.ReadAllLines(@"C:\Users\Radina\Desktop\ANUL 2\MVP\Tema1\text.txt");
            int ok = 0;
            for (int i = 0; i < vector.Length - 3; i += 4)
            {
                if ((cuvant == vector[i] && descriere == vector[i + 2]) || (cuvant == vector[i] && image.Source.ToString() == vector[i + 3]))
                {
                    if (contor < 4)
                    {
                        contor++;
                        ok = 1;
                        MessageBox.Show("Felicitari! \nAti raspuns corect! " + contor);
                        raspunsuriCorecte++;
                    }
                    else
                    {
                        if (raspunsuriCorecte == 5)
                        {
                            MessageBox.Show("Finalul Jocului!\nAti castigat!");
                            break;
                        }
                        else
                        {
                            MessageBox.Show("Finalul Jocului!\nAti raspuns corect la "+raspunsuriCorecte+" din 5 ");
                            this.Close();
                            break;
                        }
                    }
                }
            }
            if (ok == 0 && contor < 4)
            {
                contor++;
                MessageBox.Show("Raspuns gresit! " + contor);
            }

            image.Source = new BitmapImage(new Uri(@"C:\Users\Radina\Desktop\ANUL 2\MVP\Tema1\alb.jpg"));
             metoda();
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
            Quiz window = new Quiz();
            window.Show();
        }
    }
}
