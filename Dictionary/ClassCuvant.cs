﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;


namespace Tema1
{
    public class ClassCuvant: INotifyPropertyChanged
    {
        private string cuvant;
        public string Cuvant
        {
            get
            {
                return cuvant;
            }
            set
            {
                cuvant = value;
                NotifyPropertyChanged("Cuvant");
            }
        }
        private string categorie;
        public string  Categorie
        {
            get
            {
                return categorie;
            }
            set
            {
                categorie = value;
                NotifyPropertyChanged("Categorie");
            }
        }
        private string descriere;
        public string Descriere
        {
            get
            {
                return descriere;
            }
            set
            {
                descriere = value;
                NotifyPropertyChanged("Descriere");
            }
        }
        private string imageURL;
        public string ImageURL
        {
            get
            {
                return imageURL;
            }
            set
            {
                descriere = value;
                NotifyPropertyChanged("URL");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
