﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema1
{
    public class Cuvinte
    {
        public List<String> ListaCategorii { get; set; }
        public List<String> ListaCuvinte { get; set; }

        public Cuvinte()
        {
            Fisier.fileReading();
            ListaCategorii = Fisier.categorii;
            ListaCuvinte = Fisier.cuvinte;
        }

    }
   
}
