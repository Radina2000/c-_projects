﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tema1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Button1(object sender, RoutedEventArgs e)
        {
            AdminWindow w = new AdminWindow();
            w.Show();
        }

        private void Button2(object sender, RoutedEventArgs e)
        {
            Search w = new Search();
            w.Show();
        }

        private void Button3(object sender, RoutedEventArgs e)
        {
            Quiz w = new Quiz();
            w.Show();
        }
    }
}
