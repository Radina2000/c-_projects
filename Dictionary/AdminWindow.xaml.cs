﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace Tema1
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {

        public AdminWindow()
        {
            InitializeComponent();
        }
        private void ButtonAdd(object sender, RoutedEventArgs e)
        {
            Fisier.fileReading();
            String categorie;
            if (other_category.IsVisible == true)
            {
                categorie = other_category.Text;
            }
            else
            {
                categorie = category.Text;
            }
            File.AppendAllText(@"C:\Users\Radina\Desktop\ANUL 2\MVP\Tema1\text.txt", Environment.NewLine + word.Text);
            File.AppendAllText(@"C:\Users\Radina\Desktop\ANUL 2\MVP\Tema1\text.txt", Environment.NewLine + categorie);
            File.AppendAllText(@"C:\Users\Radina\Desktop\ANUL 2\MVP\Tema1\text.txt", Environment.NewLine + description.Text);          
            File.AppendAllText(@"C:\Users\Radina\Desktop\ANUL 2\MVP\Tema1\text.txt", Environment.NewLine + imagebox.Source);
            Fisier.categorii.Add(categorie);
            //Fisier.cuvinte.Add(word.Text);
            this.Close();
            AdminWindow window = new AdminWindow();
            window.Show();
        }

        private void word_TextChanged(object sender, TextChangedEventArgs e)
        {
            Fisier.fileReading();
        }

        private void ButtonModify(object sender, RoutedEventArgs e)
        {
            Fisier.fileReading();
            String categorie;
            if (category.IsVisible == true)
            {
                categorie = other_category.Text;
            }
            else
            {
                categorie = category.Text;
            }
            String cuvant = word.Text;
            String descriere = description.Text;
            string[] vector = System.IO.File.ReadAllLines(@"C:\Users\Radina\Desktop\ANUL 2\MVP\Tema1\text.txt");
            for (int i = 0; i < vector.Length-3; i +=4)
            {
                if (vector[i] != cuvant)
                    cuvant = vector[i];
                if (vector[i + 1] != categorie)
                    categorie = vector[i + 1];
                if (vector[i + 2] != descriere)
                    descriere = vector[i + 2];
            }

        }
        
         private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.jpg)|*.jpg|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
                imagebox.Source = new BitmapImage(new Uri(openFileDialog.FileName, UriKind.Absolute));
        }

        private void ButtonDelete(object sender, RoutedEventArgs e)
        {
            Fisier.fileReading();
            List<string> sterse = File.ReadAllLines(@"C:\Users\Radina\Desktop\ANUL 2\MVP\Tema1\text.txt").ToList();
            sterse.RemoveAt(Mylist.SelectedIndex);
            sterse.RemoveAt(Mylist.SelectedIndex);
            sterse.RemoveAt(Mylist.SelectedIndex);
            sterse.RemoveAt(Mylist.SelectedIndex);
            File.WriteAllLines(@"C:\Users\Radina\Desktop\ANUL 2\MVP\Tema1\text.txt", sterse);
            (DataContext as Cuvinte).ListaCuvinte.RemoveAt(Mylist.SelectedIndex);
            this.Close();          
        }
    }
}
