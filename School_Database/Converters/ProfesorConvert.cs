﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Converters
{
    class ProfesorConvert: IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != "" && values[1] != "" && values[2] != "" )
            {
                return new ProfesorVM()
                {
                    Id_profesor = int.Parse(values[0].ToString()),
                    Nume = values[1].ToString(),
                    Id_login= int.Parse(values[2].ToString()),
                    //Is_diriginte= bool.Parse(values[3].ToString())

                };
            }
            else
            {
                return null;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            ProfesorVM pers = value as ProfesorVM;
            object[] result = new object[4] {pers.Id_profesor, pers.Nume ,pers.Id_login,pers.Is_diriginte};
            return result;
        }
    }
}

