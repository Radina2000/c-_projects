﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Converters
{
    class MaterieConvert:IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != "" && values[1] != "" && values[2] != "" && values[3] != "")
            {
                return new MaterieVM()
                {
                    //Id_materie = int.Parse(values[0].ToString()),
                    Nume = values[1].ToString(),
                   // Id_login = int.Parse(values[2].ToString()),
                    // Is_diriginte= bool.Parse(values[3].ToString())

                };
            }
            else
            {
                return null;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            MaterieVM pers = value as MaterieVM;
            object[] result = new object[1] { pers.Nume };
            return result;
        }
    }
}
