﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Converters
{
    class CatalogConvert: IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (values[0] != null && values[1] != null)
            {
                return new CatalogVM()
                {
                   // Id_catalog= int.Parse(values[0].ToString()),
                   // Nota = Nullable<float>.Parse( values[1].ToString()),


                };
            }
            else
            {
                return null;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            ElevVM pers = value as ElevVM;
            object[] result = new object[1] {/*pers.Id_elev,*/pers.Nume/*,pers.Id_login ,pers.Id_clasa*/};
            return result;
        }
    }
}
