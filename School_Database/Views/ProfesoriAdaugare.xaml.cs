﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using School_Database_Tema3.Models.Actions;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Views
{
    /// <summary>
    /// Interaction logic for ProfesoriAdaugare.xaml
    /// </summary>
    public partial class ProfesoriAdaugare : Window
    {
        SchoolEntities1 ctx;
        private ElevVM elevContext;
        SqlCommand cmd;
        SqlConnection con;
        SqlDataAdapter da;
        public ProfesoriAdaugare()
        {
            InitializeComponent();
            ctx = new SchoolEntities1();
        }
        string Conn = ("Data Source=DESKTOP-6PH36P6;Initial Catalog=School;Integrated Security=true");
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection con = new SqlConnection(Conn);
            con.Open();
            cmd = new SqlCommand("INSERT INTO Profesor(id_profesor,Nume,id_login,is_diriginte) VALUES (@id_profesor,@Name,@id_login,@is_diriginte)", con);
            cmd.Parameters.Add("@id_profesor", txtId.Text);
            cmd.Parameters.Add("@Name", txtnume.Text);
            cmd.Parameters.Add("@id_login", id_logintxt.Text);
            cmd.Parameters.Add("@is_diriginte", is_diriginte.Text);
            cmd.ExecuteNonQuery();
        }
    }
}
