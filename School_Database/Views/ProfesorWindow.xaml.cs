﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using School_Database_Tema3.Models.Actions;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Views
{
    /// <summary>
    /// Interaction logic for ProfesorWindow.xaml
    /// </summary>
    public partial class ProfesorWindow : Window
    {
        SchoolEntities1 ctx;
        private ElevVM elevContext;
        SqlCommand cmd;
        SqlConnection con;
        SqlDataAdapter da;
        string Conn = ("Data Source=DESKTOP-6PH36P6;Initial Catalog=School;Integrated Security=true");
        public ProfesorWindow()
        {
            SqlConnection con = new SqlConnection(Conn);
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection con = new SqlConnection(Conn);
            try
            {
                con.Open();
                string query = "select e.nume as Nume,c.nota as Nota,m.Nume as Materie,c.absenta as Absenta,c.absenta_nemotivata as " +
                    "Absenta_Nemotivata from Catalog c inner join Elev e on c.id_elev=e.id_elev inner join Materie m on m.id_materie=c.id_materie;";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("Catalog");
                sda.Fill(dt);
                grd.ItemsSource = dt.DefaultView;
                sda.Update(dt);

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            SqlConnection con = new SqlConnection(Conn);
            con.Open();
            cmd = new SqlCommand("INSERT INTO Catalog(id_catalog,nota,absenta,absenta_nemotivata,id_materie,id_elev) VALUES (@id_elev,@nota,@absenta,@id_absnmv,@id_materie,@id_elev)", con);
            cmd.Parameters.Add("@id_catalog", txtId.Text);
            cmd.Parameters.Add("@nota", txtnota.Text);
            cmd.Parameters.Add("@absenta", absentatxt.Text);
            cmd.Parameters.Add("@id_absnmv", abs_nemtxt.Text);
            cmd.Parameters.Add("@id_materie", id_materietxt.Text);
            cmd.Parameters.Add("@id_elev", id_elevtxt.Text);
            cmd.ExecuteNonQuery();
        }
    }
}
