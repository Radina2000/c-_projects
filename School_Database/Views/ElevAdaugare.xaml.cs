﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using School_Database_Tema3.Models;
using School_Database_Tema3.Models.Actions;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Views
{
    /// <summary>
    /// Interaction logic for Admin.xaml
    /// </summary>
    public partial class Admin : Window
    {
       SchoolEntities1 ctx;
       private ElevVM elevContext;
        SqlCommand cmd;
        SqlConnection con;
        SqlDataAdapter da; 
        public Admin()
        {
            InitializeComponent();
            ctx = new SchoolEntities1();
            //adaugare.Visibility = Visibility.Hidden;
           // stergere.Visibility = Visibility.Hidden;
           // modificare.Visibility = Visibility.Hidden;

        }

        string Conn = ("Data Source=DESKTOP-6PH36P6;Initial Catalog=School;Integrated Security=true");
        SchoolEntities1 context = new SchoolEntities1();
      //  private ElevVM elevContext;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection con = new SqlConnection(Conn);
            con.Open();
            cmd = new SqlCommand("INSERT INTO Elev(id_elev,Nume,id_login,id_clasa) VALUES (@id_elev,@Name,@id_login,@id_clasa)", con);
            cmd.Parameters.Add("@id_elev", txtId.Text);
            cmd.Parameters.Add("@Name", txtnum.Text);
            cmd.Parameters.Add("@id_login", id_logintxt.Text);
            cmd.Parameters.Add("@id_clasa", id_Clasatxt.Text);
            cmd.ExecuteNonQuery();         
            //ElevVM.ElevList.ItemsSource = elevs;
        }
}
}
