﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using School_Database_Tema3.Models.Actions;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Views
{
    /// <summary>
    /// Interaction logic for MaterieWindow.xaml
    /// </summary>
    public partial class MaterieWindow : Window
    {
        SchoolEntities1 ctx;
        private MaterieVM elevContext;
        SqlCommand cmd;
        SqlConnection con;
        SqlDataAdapter da;
        public MaterieWindow()
        {
            InitializeComponent();
            ctx = new SchoolEntities1();
        }
        string Conn = ("Data Source=DESKTOP-6PH36P6;Initial Catalog=School;Integrated Security=true");
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection con = new SqlConnection(Conn);
            con.Open();
            cmd = new SqlCommand("INSERT INTO Materie(id_materie,Nume,id_profesor,id_profil,este_teza) VALUES (@id_materie,@Name,@id_profesor,@id_profil,@este_teza)", con);
            cmd.Parameters.Add("@id_materie", txtId.Text);
            cmd.Parameters.Add("@Name", txtnume.Text);
            cmd.Parameters.Add("@id_profesor", id_proftxt.Text);
            cmd.Parameters.Add("@id_profil", id_profiltxt.Text);
            cmd.Parameters.Add("@este_teza", este_teza.Text);
            cmd.ExecuteNonQuery();
        }
    }
}
