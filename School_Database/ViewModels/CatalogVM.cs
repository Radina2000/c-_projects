﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using School_Database_Tema3.Helper;
using School_Database_Tema3.Models.Actions;

namespace School_Database_Tema3.ViewModels
{
    class CatalogVM:BaseVM
    {
        CatalogActions cAct;
        // MaterieActions mAct;
        
        ElevActions eAct;

        public CatalogVM()
        {
            cAct = new CatalogActions(this);
            eAct = new ElevActions(null);
            //cAct = new ClasaActions(this);
        }

        #region Data Members

        private int id_catalog;
        private Nullable<float> nota;
        private Nullable<System.DateTime> absenta;
        private Nullable<System.DateTime> absenta_nemotivata;
        private int id_materie;
        private int id_elev;
        private string message;
        private ObservableCollection<CatalogVM> catalogList;
        private ObservableCollection<ElevVM> elevList;
        // private ObservableCollection<ClasaVM> clasaList;



        public int Id_catalog
        {
            get
            {
                return id_catalog;
            }
            set
            {
                id_catalog = value;
                NotifyPropertyChanged("id_catalog");
            }
        }

        public Nullable<float> Nota
        {
            get
            {
                return nota;
            }
            set
            {
                nota = value;
                NotifyPropertyChanged("nota");
            }
        }

        public Nullable<System.DateTime> Absenta
        {
            get
            {
                return absenta;
            }
            set
            {
                absenta = value;
                NotifyPropertyChanged("absenta");
            }
        }

        public Nullable<System.DateTime> Absenta_nemotivata
        {
            get
            {
                return absenta_nemotivata;
            }
            set
            {
                absenta_nemotivata = value;
                NotifyPropertyChanged("absenta_nemotivata");
            }
        }
        public int Id_materie
        {
            get
            {
                return id_materie;
            }
            set
            {
                id_materie = value;
                NotifyPropertyChanged("id_materie");
            }
        }
        public int Id_elev
        {
            get
            {
                return id_elev;
            }
            set
            {
                id_elev = value;
                NotifyPropertyChanged("id_elev");
            }
        }
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }
        public ObservableCollection<CatalogVM> CatalogList
        {
            get
            {
                catalogList = cAct.AllCatalog();
                return catalogList;
            }
            set
            {
                catalogList = value;
                NotifyPropertyChanged("CatalogList");
            }
        }
        public ObservableCollection<ElevVM> ElevList
        {
            get
            {
                elevList = eAct.AllElev();
                return elevList;
            }
            set
            {
                elevList = value;
                NotifyPropertyChanged("PersonsList");
            }
        }

        #endregion
        #region Command Members

        //asta este pt butonul Add
        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new RelayCommand(cAct.AddMethod);
                }
                return addCommand;
            }
        }

        //asta este pt butonul Update
        private ICommand updateCommand;
        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new RelayCommand(cAct.UpdateMethod);
                }
                return updateCommand;
            }
        }

        //asta este pt butonul Delete
        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new RelayCommand(cAct.DeleteMethod);
                }
                return deleteCommand;
            }
        }

        #endregion
    }
}

