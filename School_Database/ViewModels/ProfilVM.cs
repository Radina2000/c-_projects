﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School_Database_Tema3.Helper;
using School_Database_Tema3.Models.Actions;

namespace School_Database_Tema3.ViewModels
{
    class ProfilVM:BaseVM
    {
        ProfilActions mAct;

        public ProfilVM()
        {
            mAct = new ProfilActions(this);
        }

        #region Data Members

        private int id_profil;
        private string nume;
        private string message;
        private ObservableCollection<ProfilVM> profilList;
        public int Id_profil
        {
            get
            {
                return id_profil;
            }
            set
            {
                id_profil = value;
                NotifyPropertyChanged("id_profil");
            }
        }

        public string Nume
        {
            get
            {
                return nume;
            }
            set
            {
                nume = value;
                NotifyPropertyChanged("nume");
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }
        public ObservableCollection<ProfilVM>ProfilList 
        {
            get
            {
                profilList = mAct.AllProfil();
                return profilList;
            }
            set
            {
                profilList = value;
                NotifyPropertyChanged("ProfesorList");
            }
        }

        /* #endregion
         #region Command Members

         //asta este pt butonul Add
         private ICommand addCommand;
         public ICommand AddCommand
         {
             get
             {
                 if (addCommand == null)
                 {
                     addCommand = new RelayCommand(mAct.AddMethod);
                 }
                 return addCommand;
             }
         }

         //asta este pt butonul Update
         private ICommand updateCommand;
         public ICommand UpdateCommand
         {
             get
             {
                 if (updateCommand == null)
                 {
                     updateCommand = new RelayCommand(mAct.UpdateMethod);
                 }
                 return updateCommand;
             }
         }

         //asta este pt butonul Delete
         private ICommand deleteCommand;
         public ICommand DeleteCommand
         {
             get
             {
                 if (deleteCommand == null)
                 {
                     deleteCommand = new RelayCommand(mAct.DeleteMethod);
                 }
                 return deleteCommand;
             }
         }*/

        #endregion
    }
}
