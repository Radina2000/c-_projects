﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using School_Database_Tema3.Helper;
using School_Database_Tema3.Models.Actions;

namespace School_Database_Tema3.ViewModels
{
    class ElevVM: BaseVM
    {
        ElevActions eAct;
        LoginActions lAct;
       // ClasaActions cAct;

        public ElevVM()
        {
            eAct = new ElevActions(this);
            lAct = new LoginActions(null);
            //cAct = new ClasaActions(this);
        }

        #region Data Members

        private int id_elev;
        private string nume;
        private int id_login;
        private int id_clasa;
        private string message;
        private ObservableCollection<ElevVM> elevList;
        private ObservableCollection<LoginVM> loginList;
       // private ObservableCollection<ClasaVM> clasaList;



        public int Id_elev
        {
            get
            {
                return id_elev;
            }
            set
            {
                id_elev = value;
                NotifyPropertyChanged("id_elev");
            }
        }

        public string Nume
        {
            get
            {
                return nume;
            }
            set
            {
                nume = value;
                NotifyPropertyChanged("nume");
            }
        }

        public int Id_login
        {
            get
            {
                return id_login;
            }
            set
            {
                id_login = value;
                NotifyPropertyChanged("id_login");
            }
        }

        public int Id_clasa
        {
            get
            {
                return id_clasa;
            }
            set
            {
                id_clasa = value;
                NotifyPropertyChanged("id_clasa");
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }
        public ObservableCollection<ElevVM> ElevList
        {
            get
            {
                elevList = eAct.AllElev();
                return elevList;
            }
            set
            {
                elevList = value;
                NotifyPropertyChanged("PersonsList");
            }
        }
        public ObservableCollection<LoginVM> LoginList
        {
            get
            {
                loginList = lAct.AllLogin();
                return loginList;
            }
            set
            {
                loginList = value;
                NotifyPropertyChanged("LoginList");
            }
        }

        #endregion
        #region Command Members

        //asta este pt butonul Add
        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new RelayCommand(eAct.AddMethod);
                }
                return addCommand;
            }
        }

        //asta este pt butonul Update
        private ICommand updateCommand;
        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new RelayCommand(eAct.UpdateMethod);
                }
                return updateCommand;
            }
        }

        //asta este pt butonul Delete
        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new RelayCommand(eAct.DeleteMethod);
                }
                return deleteCommand;
            }
        }

        private ICommand elevSelectCommand;
        public ICommand ElevSelectCommand
        {
            get
            {
                if (elevSelectCommand == null)
                {
                    elevSelectCommand = new RelayCommand(eAct.Elevi_Select);
                }
                return elevSelectCommand;
            }
        }

        #endregion
    }
}

