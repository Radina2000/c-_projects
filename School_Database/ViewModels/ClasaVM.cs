﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School_Database_Tema3.Helper;
using School_Database_Tema3.Models.Actions;

namespace School_Database_Tema3.ViewModels
{
    class ClasaVM:BaseVM
    {
        ClasaActions mAct;

        public  ClasaVM()
        {
            mAct = new ClasaActions(this);
        }

        #region Data Members

        private int id_clasa;
        private string nume;
        private int id_profil;
        private int id_profesor;
        private string message;
        private ObservableCollection<ClasaVM> clasaList;
        public int Id_clasa
        {
            get
            {
                return id_clasa;
            }
            set
            {
                id_clasa = value;
                NotifyPropertyChanged("id_profesor");
            }
        }

        public string Nume
        {
            get
            {
                return nume;
            }
            set
            {
                nume = value;
                NotifyPropertyChanged("nume");
            }
        }
        public int Id_profil
        {
            get
            {
                return id_profil;
            }
            set
            {
                id_profil = value;
                NotifyPropertyChanged("id_profil");
            }
        }
        public int Id_profesor
        {
            get
            {
                return id_profesor;
            }
            set
            {
                id_profesor = value;
                NotifyPropertyChanged("id_profesor");
            }
        }
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }

        public ObservableCollection<ClasaVM> ClasaList
        {
            get
            {
                clasaList = mAct.AllClasa();
                return clasaList;
            }
            set
            {
                clasaList = value;
                NotifyPropertyChanged("ClasaList");
            }
        }

        /* #endregion
         #region Command Members

         //asta este pt butonul Add
         private ICommand addCommand;
         public ICommand AddCommand
         {
             get
             {
                 if (addCommand == null)
                 {
                     addCommand = new RelayCommand(mAct.AddMethod);
                 }
                 return addCommand;
             }
         }

         //asta este pt butonul Update
         private ICommand updateCommand;
         public ICommand UpdateCommand
         {
             get
             {
                 if (updateCommand == null)
                 {
                     updateCommand = new RelayCommand(mAct.UpdateMethod);
                 }
                 return updateCommand;
             }
         }

         //asta este pt butonul Delete
         private ICommand deleteCommand;
         public ICommand DeleteCommand
         {
             get
             {
                 if (deleteCommand == null)
                 {
                     deleteCommand = new RelayCommand(mAct.DeleteMethod);
                 }
                 return deleteCommand;
             }
         }*/
        #endregion
    }
}
