﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using School_Database_Tema3.Helper;
using School_Database_Tema3.Models.Actions;

namespace School_Database_Tema3.ViewModels
{
    class LoginVM:BaseVM
    {
        LoginActions lAct;

        public LoginVM()
        {
            lAct = new LoginActions(this);
           
        }

        #region Data Members

        private int id_login;
        private string nume;
        private string parola;
        private string message;
        private ObservableCollection<LoginVM> loginList;
        public int Id_login
        {
            get
            {
                return id_login;
            }
            set
            {
                id_login = value;
                NotifyPropertyChanged("id_login");
            }
        }

        public string Nume
        {
            get
            {
                return nume;
            }
            set
            {
                nume = value;
                NotifyPropertyChanged("nume");
            }
        }

        public string Parola
        {
            get
            {
                return parola;
            }
            set
            {
                parola = value;
                NotifyPropertyChanged("parola");
            }
        }

       
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }
        public ObservableCollection<LoginVM> LoginList
        {
            get
            {
                loginList = lAct.AllLogin();
                return loginList;
            }
            set
            {
                loginList = value;
                NotifyPropertyChanged("LoginList");
            }
        }

        #endregion
        #region Command Members

        //asta este pt butonul Add
        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new RelayCommand(lAct.AddMethod);
                }
                return addCommand;
            }
        }

        //asta este pt butonul Update
        private ICommand updateCommand;
        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new RelayCommand(lAct.UpdateMethod);
                }
                return updateCommand;
            }
        }

        //asta este pt butonul Delete
        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new RelayCommand(lAct.DeleteMethod);
                }
                return deleteCommand;
            }
        }

        #endregion
    }
}
