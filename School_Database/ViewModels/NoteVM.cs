﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using School_Database_Tema3.Helper;
using School_Database_Tema3.Models.Actions;

namespace School_Database_Tema3.ViewModels
{
    class NoteVM:BaseVM
    {
        NoteActions cAct;


        public NoteVM()
        {
            cAct = new NoteActions(this);
            // eAct = new ElevActions(null);
            //cAct = new ClasaActions(this);
        }

        #region Data Members

        private int id_nota;
        private double nota;
        private int id_materie;
        private int id_elev;
        private string message;
        private ObservableCollection<NoteVM> notaList;
        // private ObservableCollection<ElevVM> elevList;
        // private ObservableCollection<ClasaVM> clasaList;



        public int Id_nota
        {
            get
            {
                return id_nota;
            }
            set
            {
                id_nota = value;
                NotifyPropertyChanged("id_nota");
            }
        }

        public double Nota
        {
            get
            {
                return nota;
            }
            set
            {
                nota = value;
                NotifyPropertyChanged("nota");
            }
        }


        public int Id_materie
        {
            get
            {
                return id_materie;
            }
            set
            {
                id_materie = value;
                NotifyPropertyChanged("id_materie");
            }
        }
        public int Id_elev
        {
            get
            {
                return id_elev;
            }
            set
            {
                id_elev = value;
                NotifyPropertyChanged("id_elev");
            }
        }
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }
        public ObservableCollection<NoteVM> NotaList
        {
            get
            {
                notaList = cAct.AllNota();
                return notaList;
            }
            set
            {
                notaList = value;
                NotifyPropertyChanged("notaList");
            }
        }


        #endregion
        #region Command Members

        //asta este pt butonul Add
        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new RelayCommand(cAct.AddMethod);
                }
                return addCommand;
            }
        }

        //asta este pt butonul Update
        private ICommand updateCommand;
        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    // updateCommand = new RelayCommand(cAct.UpdateMethod);
                }
                return updateCommand;
            }
        }

        //asta este pt butonul Delete
        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    //deleteCommand = new RelayCommand(cAct.DeleteMethod);
                }
                return deleteCommand;
            }
        }

        #endregion
    }
}

