﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using School_Database_Tema3.Helper;
using School_Database_Tema3.Models.Actions;

namespace School_Database_Tema3.ViewModels
{
    class ProfesorVM:BaseVM
    {
        ProfesorActions mAct;

        public ProfesorVM()
        {
            mAct = new ProfesorActions(this);
        }

        #region Data Members

        private int id_profesor;
        private string nume;
        private int id_login;
        private bool is_diriginte;
        private string message;
        private ObservableCollection<ProfesorVM> profesorList;
        public int Id_profesor
        {
            get
            {
                return id_profesor;
            }
            set
            {
                id_profesor = value;
                NotifyPropertyChanged("id_profesor");
            }
        }

        public string Nume
        {
            get
            {
                return nume;
            }
            set
            {
                nume = value;
                NotifyPropertyChanged("nume");
            }
        }
        public int Id_login
        {
            get
            {
                return id_login;
            }
            set
            {
                id_login = value;
                NotifyPropertyChanged("id_login");
            }
        }
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }
        public bool Is_diriginte
        {
            get
            {
                return is_diriginte;
            }
            set
            {
                is_diriginte = value;
                NotifyPropertyChanged("is_diriginte");
            }
        }
        public ObservableCollection<ProfesorVM> ProfesorList
        {
            get
            {
                profesorList = mAct.AllProfesor();
                return profesorList;
            }
            set
            {
                profesorList = value;
                NotifyPropertyChanged("ProfesorList");
            }
        }

         #endregion
         #region Command Members

         //asta este pt butonul Add
         private ICommand addCommand;
         public ICommand AddCommand
         {
             get
             {
                 if (addCommand == null)
                 {
                     addCommand = new RelayCommand(mAct.AddMethod);
                 }
                 return addCommand;
             }
         }
        
         //asta este pt butonul Update
         private ICommand updateCommand;
         public ICommand UpdateCommand
         {
             get
             {
                 if (updateCommand == null)
                 {
                     updateCommand = new RelayCommand(mAct.UpdateMethod);
                 }
                 return updateCommand;
             }
         }

         //asta este pt butonul Delete
         private ICommand deleteCommand;
         public ICommand DeleteCommand
         {
             get
             {
                 if (deleteCommand == null)
                 {
                     deleteCommand = new RelayCommand(mAct.DeleteMethod);
                 }
                 return deleteCommand;
             }
         }
        #endregion
    }
}
