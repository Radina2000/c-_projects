﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using School_Database_Tema3.Helper;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Models.Actions
{
    class ElevActions : BaseVM
    {
        SqlCommand cmd;
        SqlConnection con;
        SqlDataAdapter da;
        string Conn = ("Data Source=DESKTOP-6PH36P6;Initial Catalog=School;Integrated Security=true");
        SchoolEntities1 context = new SchoolEntities1();

        private ElevVM elevContext;
        public ElevActions(ElevVM elevContext)
        {
            this.elevContext = elevContext;
        }
        public ObservableCollection<ElevVM> AllElev()
        {
            List<Elev> elevs = context.Elevs.ToList();
            ObservableCollection<ElevVM> result = new ObservableCollection<ElevVM>();
            foreach (Elev elv in elevs)
            {
                result.Add(new ElevVM()
                {
                    Id_elev = elv.id_elev,
                    Nume = elv.Nume,
                    Id_login = elv.id_login,
                    Id_clasa = elv.id_clasa
                });
            }
            return result;
        }
        public ObservableCollection<ElevVM> ElevesForLogin(int id_login,int id_Clasa)
        {
            List<Elev> phones = context.Elevs.Where(item =>( item.id_login == id_login) && (item.id_clasa==id_Clasa)).ToList();
            ObservableCollection<ElevVM> result = new ObservableCollection<ElevVM>();
            foreach (Elev ph in phones)
            {
                result.Add(new ElevVM()
                {
                    Id_clasa = ph.id_clasa,
                    Nume = ph.Nume,
                    Id_elev = ph.id_elev,
                    Id_login=ph.id_login
                });
            }
            return result;
        }
        public void AddMethod(object obj)
        {

            ElevVM phoneVM = obj as ElevVM;
            if (phoneVM != null)
            {
                if (String.IsNullOrEmpty(phoneVM.Nume))
                    elevContext.Message = "Numele nu este bun";

                else
                {
                    context.AddElev(phoneVM.Id_elev, phoneVM.Nume, phoneVM.Id_login, phoneVM.Id_clasa);
                    context.SaveChanges();
                    elevContext.ElevList = ElevesForLogin(phoneVM.Id_login, phoneVM.Id_clasa);
                    elevContext.Message = "";
                }
            }
        }
        public void UpdateMethod(object obj)
        {
            ElevVM loginVM = obj as ElevVM;
            if (loginVM == null)
            {
                elevContext.Message = "Selecteaza un elev";
            }
            else
            {
                context.ModifyElev(loginVM.Id_elev, loginVM.Nume, loginVM.Id_login, loginVM.Id_clasa);
                context.SaveChanges();
                elevContext.Message = "";
            }
        }

        public void DeleteMethod(object obj)
        {

            ElevVM loginVM = obj as ElevVM;
            if (loginVM == null)
            {
                elevContext.Message = "Selecteaza un elev";
            }
            else
            {
                 Elev login = context.Elevs.Where(i => i.id_elev == loginVM.Id_elev).FirstOrDefault();
                //de facut metoda daca am nevoie
                context.DeleteElev(loginVM.Id_elev);
                context.SaveChanges();
                elevContext.ElevList = AllElev();
                elevContext.Message = "";
            }
        }
        public void Elevi_Select(object obj)
        {
           // List<Elev> elevs = context.ElevSelect().ToList();

            ElevVM loginVM = obj as ElevVM;
            if (loginVM == null)
            {
                elevContext.Message = "Selecteaza un elev";
            }
            else
            {
                //Elev login = context.Elevs.Where(i => i.id_elev == loginVM.Id_elev).FirstOrDefault();
                //de facut metoda daca am nevoie
                context.ElevSelect();
                context.SaveChanges();
                List<String> list = new List<string>();
               // list = context.ElevSelect();
                elevContext.Message = "";
            }
        }

    }
}