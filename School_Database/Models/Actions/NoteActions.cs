﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School_Database_Tema3.Helper;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Models.Actions
{
    class NoteActions:BaseVM
    {
        SchoolEntities1 context = new SchoolEntities1();
        private NoteVM catalogContext;
        public NoteActions(NoteVM catalogContext)
        {
            this.catalogContext = catalogContext;
        }
        public ObservableCollection<NoteVM> AllNota()
        {
            List<Note> catalogs = context.Notes.ToList();
            ObservableCollection<NoteVM> result = new ObservableCollection<NoteVM>();
            foreach (Note ctg in catalogs)
            {
                result.Add(new NoteVM()
                {
                    Id_nota = ctg.id_nota,
                    Nota = ctg.nota,
                    Id_materie = ctg.id_materie,
                    Id_elev = ctg.id_elev
                });
            }
            return result;
        }
        public void AddMethod(object obj)
        {
            //parametrul obj este cel dat prin CommandParameter cu MultipleBinding la Button in xaml
            NoteVM catalogVM = obj as NoteVM;
            if (catalogVM != null)
            {

                // context.Add(personVM.Name, personVM.Address);
                //fara a utiliza procedura stocata AddPerson
                context.Notes.Add(new Note()
                {
                    id_nota = catalogVM.Id_nota,
                    nota = catalogVM.Nota,
                    id_materie = catalogVM.Id_materie,
                    id_elev = catalogVM.Id_elev
                });
                context.SaveChanges();
                catalogContext.NotaList = AllNota();
                catalogContext.Message = "";

            }
        }
        /* public void UpdateMethod(object obj)
         {
             NoteVM catalogVM = obj as NoteVM;
             if (catalogVM == null)
             {
                 catalogContext.Message = "Selecteaza un catalog";
             }
             else
             {
                 context.ModifyCatalog(catalogVM.Id_catalog, catalogVM.Nota, catalogVM.Absenta,
                     catalogVM.Absenta_nemotivata, catalogVM.Id_materie, catalogVM.Id_elev);
                 context.SaveChanges();
                 catalogContext.Message = "";
             }
         }

         public void DeleteMethod(object obj)
         {
             CatalogVM elevVM = obj as CatalogVM;
             if (elevVM == null)
             {
                 catalogContext.Message = "Selecteaza un catalog";
             }
             else
             {
                 Catalog catalog = context.Catalogs.Where(i => i.id_catalog == elevVM.Id_catalog).FirstOrDefault();
                 context.DeleteElev(elevVM.Id_catalog);
                 context.SaveChanges();
                 catalogContext.CatalogList = AllCatalog();
                 catalogContext.Message = "";
             }
         }*/
    }
}

