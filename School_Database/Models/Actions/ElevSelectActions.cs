﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using School_Database_Tema3.Helper;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Models.Actions
{
    class ElevSelectActions:BaseVM
    {
        SqlCommand cmd;
        SqlConnection con;
        SqlDataAdapter da;
        string Conn = ("Data Source=DESKTOP-6PH36P6;Initial Catalog=School;Integrated Security=true");
        SchoolEntities1 context = new SchoolEntities1();

        private ElevSelectVM elevContext;
        public ElevSelectActions(ElevSelectVM elevContext)
        {
            this.elevContext = elevContext;
        }
        public ObservableCollection<ElevSelect_Result> AllElevSelect()
        {
            var persons = context.ElevSelect();
          //  List<ElevSelect_Result> elevs = context.ElevSelect().ToList();
            ObservableCollection<ElevSelect_Result> result = new ObservableCollection<ElevSelect_Result>();
            foreach (var elv in persons)
            {
                result.Add(new ElevSelect_Result()
                {
                    Nume = elv.Nume,
                    Nota = elv.Nota,
                    Absenta = elv.Absenta,
                    Absenta_Nemotivata=elv.Absenta_Nemotivata
                });
            }
           return result;
        }
       
       /* public void Elevi_Select(object obj)
        {
            // List<Elev> elevs = context.ElevSelect().ToList();
            MessageBox.Show("da");
            ElevVM loginVM = obj as ElevVM;
            if (loginVM == null)
            {
                elevContext.Message = "Selecteaza un elev";
            }
            else
            {
                //Elev login = context.Elevs.Where(i => i.id_elev == loginVM.Id_elev).FirstOrDefault();
                //de facut metoda daca am nevoie
                context.ElevSelect();
                context.SaveChanges();
                List<String> list = new List<string>();
                // list = context.ElevSelect();
                elevContext.Message = "";
            }
        }*/
    }
}
