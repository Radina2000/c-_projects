﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using School_Database_Tema3.Helper;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Models.Actions
{
    class AdminActions:BaseVM
    {
        SchoolEntities1 context = new SchoolEntities1();
        private AdminVM adminContext;

        public AdminActions(AdminVM adminContext)
        {
            this.adminContext = adminContext;
        }
        public void AddMethod(object obj)
        {
           
            /* MessageBox.Show("da");
             //parametrul obj este cel dat prin CommandParameter cu MultipleBinding la Button in xaml
             AdminVM adminVM = obj as AdminVM;
             if (adminVM != null)
             {
                 if (String.IsNullOrEmpty(adminVM.Nume))
                 {
                     adminContext.Message = "Numele adminului trebuie precizat";
                 }
                 else
                 {
                     context.Admins.Add(new Admin() { id_admin=adminVM.Id_login,Nume=adminVM.Nume,id_login=adminVM.Id_login});
                     //fara a utiliza procedura stocata AddPerson
                     //context.People.Add(new Person() { Name = personVM.Name , Address = personVM.Address});
                     context.SaveChanges();
                     adminContext.AdminList = AllAdmins();
                     adminContext.Message = "";
                 }
             }*/
        }
        public void UpdateMethod(object obj)
        {
            AdminVM adminVM = obj as AdminVM;
            if (adminVM == null)
            {
                adminContext.Message = "Selecteaza un admin";
            }
            else if (String.IsNullOrEmpty(adminVM.Nume))
            {
                adminContext.Message = "Numele adminului trebuie precizat";
            }
            else
            {
                context.ModifyAdmin(adminVM.Id_admin, adminVM.Nume, adminVM.Id_login);
                context.SaveChanges();
                adminContext.Message = "";
            }
        }
        public void DeleteMethod(object obj)
        {
            AdminVM adminVM = obj as AdminVM;
            if (adminVM == null)
            {
                adminContext.Message = "Selecteaza un admin";
            }
            else
            {
                Admin adm = context.Admins.Where(i => i.id_admin == adminVM.Id_admin).FirstOrDefault();            
                    context.DeleteAdmin(adminVM.Id_admin);
                    context.SaveChanges();
                    adminContext.AdminList = AllAdmins();
                    adminContext.Message = "";

            }
        }
        public ObservableCollection<AdminVM> AllAdmins()
        {
            List<Admin> admins = context.Admins.ToList();
            ObservableCollection<AdminVM> result = new ObservableCollection<AdminVM>();
            foreach (Admin admin in admins)
            {
                result.Add(new AdminVM()
                {
                    Id_admin = admin.id_admin,
                    Nume = admin.Nume,
                    Id_login=admin.id_login
                });
            }
            return result;
        }
    }
}
