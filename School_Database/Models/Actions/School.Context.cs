﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace School_Database_Tema3.Models.Actions
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class SchoolEntities1 : DbContext
    {
        public SchoolEntities1()
            : base("name=SchoolEntities1")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Admin> Admins { get; set; }
        public virtual DbSet<Catalog> Catalogs { get; set; }
        public virtual DbSet<Clasa> Clasas { get; set; }
        public virtual DbSet<Elev> Elevs { get; set; }
        public virtual DbSet<Login> Logins { get; set; }
        public virtual DbSet<Materie> Materies { get; set; }
        public virtual DbSet<Profesor> Profesors { get; set; }
        public virtual DbSet<Profil> Profils { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<Absenta> Absentas { get; set; }
        public virtual DbSet<Note> Notes { get; set; }
    
        public virtual int AddAdmin(Nullable<int> id_admin, string nume, Nullable<int> id_login)
        {
            var id_adminParameter = id_admin.HasValue ?
                new ObjectParameter("id_admin", id_admin) :
                new ObjectParameter("id_admin", typeof(int));
    
            var numeParameter = nume != null ?
                new ObjectParameter("nume", nume) :
                new ObjectParameter("nume", typeof(string));
    
            var id_loginParameter = id_login.HasValue ?
                new ObjectParameter("id_login", id_login) :
                new ObjectParameter("id_login", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddAdmin", id_adminParameter, numeParameter, id_loginParameter);
        }
    
        public virtual int AddCatalog(Nullable<int> id_catalog, Nullable<float> nota, Nullable<System.DateTime> absenta, Nullable<System.DateTime> absenta_nemotivata, Nullable<int> id_materie, Nullable<int> id_elev)
        {
            var id_catalogParameter = id_catalog.HasValue ?
                new ObjectParameter("id_catalog", id_catalog) :
                new ObjectParameter("id_catalog", typeof(int));
    
            var notaParameter = nota.HasValue ?
                new ObjectParameter("nota", nota) :
                new ObjectParameter("nota", typeof(float));
    
            var absentaParameter = absenta.HasValue ?
                new ObjectParameter("absenta", absenta) :
                new ObjectParameter("absenta", typeof(System.DateTime));
    
            var absenta_nemotivataParameter = absenta_nemotivata.HasValue ?
                new ObjectParameter("absenta_nemotivata", absenta_nemotivata) :
                new ObjectParameter("absenta_nemotivata", typeof(System.DateTime));
    
            var id_materieParameter = id_materie.HasValue ?
                new ObjectParameter("id_materie", id_materie) :
                new ObjectParameter("id_materie", typeof(int));
    
            var id_elevParameter = id_elev.HasValue ?
                new ObjectParameter("id_elev", id_elev) :
                new ObjectParameter("id_elev", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddCatalog", id_catalogParameter, notaParameter, absentaParameter, absenta_nemotivataParameter, id_materieParameter, id_elevParameter);
        }
    
        public virtual int AddClass(Nullable<int> id_clasa, string nume, Nullable<int> id_profil, Nullable<int> id_diriginte)
        {
            var id_clasaParameter = id_clasa.HasValue ?
                new ObjectParameter("id_clasa", id_clasa) :
                new ObjectParameter("id_clasa", typeof(int));
    
            var numeParameter = nume != null ?
                new ObjectParameter("nume", nume) :
                new ObjectParameter("nume", typeof(string));
    
            var id_profilParameter = id_profil.HasValue ?
                new ObjectParameter("id_profil", id_profil) :
                new ObjectParameter("id_profil", typeof(int));
    
            var id_diriginteParameter = id_diriginte.HasValue ?
                new ObjectParameter("id_diriginte", id_diriginte) :
                new ObjectParameter("id_diriginte", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddClass", id_clasaParameter, numeParameter, id_profilParameter, id_diriginteParameter);
        }
    
        public virtual int AddElev(Nullable<int> id, string nume, Nullable<int> id_login, Nullable<int> id_clasa)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("id", id) :
                new ObjectParameter("id", typeof(int));
    
            var numeParameter = nume != null ?
                new ObjectParameter("nume", nume) :
                new ObjectParameter("nume", typeof(string));
    
            var id_loginParameter = id_login.HasValue ?
                new ObjectParameter("id_login", id_login) :
                new ObjectParameter("id_login", typeof(int));
    
            var id_clasaParameter = id_clasa.HasValue ?
                new ObjectParameter("id_clasa", id_clasa) :
                new ObjectParameter("id_clasa", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddElev", idParameter, numeParameter, id_loginParameter, id_clasaParameter);
        }
    
        public virtual int AddProfesor(Nullable<int> id, string nume, Nullable<int> id_login, Nullable<bool> is_diriginte)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("id", id) :
                new ObjectParameter("id", typeof(int));
    
            var numeParameter = nume != null ?
                new ObjectParameter("nume", nume) :
                new ObjectParameter("nume", typeof(string));
    
            var id_loginParameter = id_login.HasValue ?
                new ObjectParameter("id_login", id_login) :
                new ObjectParameter("id_login", typeof(int));
    
            var is_diriginteParameter = is_diriginte.HasValue ?
                new ObjectParameter("is_diriginte", is_diriginte) :
                new ObjectParameter("is_diriginte", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddProfesor", idParameter, numeParameter, id_loginParameter, is_diriginteParameter);
        }
    
        public virtual int DeleteAdmin(Nullable<int> id)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("id", id) :
                new ObjectParameter("id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteAdmin", idParameter);
        }
    
        public virtual int DeleteCatalog(Nullable<int> id)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("id", id) :
                new ObjectParameter("id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteCatalog", idParameter);
        }
    
        public virtual int DeleteElev(Nullable<int> id)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("id", id) :
                new ObjectParameter("id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteElev", idParameter);
        }
    
        public virtual int DeleteProfesor(Nullable<int> id)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("id", id) :
                new ObjectParameter("id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteProfesor", idParameter);
        }
    
        public virtual ObjectResult<ElevSelect_Result> ElevSelect()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<ElevSelect_Result>("ElevSelect");
        }
    
        public virtual ObjectResult<GetAllProfesorsFromClass_Result> GetAllProfesorsFromClass()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAllProfesorsFromClass_Result>("GetAllProfesorsFromClass");
        }
    
        public virtual ObjectResult<string> GetMateriiCuTeza()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("GetMateriiCuTeza");
        }
    
        public virtual ObjectResult<GetNotes_5ForElev_Result> GetNotes_5ForElev(Nullable<int> persID)
        {
            var persIDParameter = persID.HasValue ?
                new ObjectParameter("persID", persID) :
                new ObjectParameter("persID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetNotes_5ForElev_Result>("GetNotes_5ForElev", persIDParameter);
        }
    
        public virtual int ModifyAdmin(Nullable<int> iD, string nume, Nullable<int> id_login)
        {
            var iDParameter = iD.HasValue ?
                new ObjectParameter("ID", iD) :
                new ObjectParameter("ID", typeof(int));
    
            var numeParameter = nume != null ?
                new ObjectParameter("nume", nume) :
                new ObjectParameter("nume", typeof(string));
    
            var id_loginParameter = id_login.HasValue ?
                new ObjectParameter("id_login", id_login) :
                new ObjectParameter("id_login", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("ModifyAdmin", iDParameter, numeParameter, id_loginParameter);
        }
    
        public virtual int ModifyCatalog(Nullable<int> iD, Nullable<float> nota, Nullable<System.DateTime> absenta, Nullable<System.DateTime> absenta_nemotivata, Nullable<int> id_materie, Nullable<int> id_elev)
        {
            var iDParameter = iD.HasValue ?
                new ObjectParameter("ID", iD) :
                new ObjectParameter("ID", typeof(int));
    
            var notaParameter = nota.HasValue ?
                new ObjectParameter("nota", nota) :
                new ObjectParameter("nota", typeof(float));
    
            var absentaParameter = absenta.HasValue ?
                new ObjectParameter("absenta", absenta) :
                new ObjectParameter("absenta", typeof(System.DateTime));
    
            var absenta_nemotivataParameter = absenta_nemotivata.HasValue ?
                new ObjectParameter("absenta_nemotivata", absenta_nemotivata) :
                new ObjectParameter("absenta_nemotivata", typeof(System.DateTime));
    
            var id_materieParameter = id_materie.HasValue ?
                new ObjectParameter("id_materie", id_materie) :
                new ObjectParameter("id_materie", typeof(int));
    
            var id_elevParameter = id_elev.HasValue ?
                new ObjectParameter("id_elev", id_elev) :
                new ObjectParameter("id_elev", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("ModifyCatalog", iDParameter, notaParameter, absentaParameter, absenta_nemotivataParameter, id_materieParameter, id_elevParameter);
        }
    
        public virtual int ModifyElev(Nullable<int> iD, string nume, Nullable<int> id_login, Nullable<int> id_clasa)
        {
            var iDParameter = iD.HasValue ?
                new ObjectParameter("ID", iD) :
                new ObjectParameter("ID", typeof(int));
    
            var numeParameter = nume != null ?
                new ObjectParameter("nume", nume) :
                new ObjectParameter("nume", typeof(string));
    
            var id_loginParameter = id_login.HasValue ?
                new ObjectParameter("id_login", id_login) :
                new ObjectParameter("id_login", typeof(int));
    
            var id_clasaParameter = id_clasa.HasValue ?
                new ObjectParameter("id_clasa", id_clasa) :
                new ObjectParameter("id_clasa", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("ModifyElev", iDParameter, numeParameter, id_loginParameter, id_clasaParameter);
        }
    
        public virtual int ModifyLogin(Nullable<int> iD, string nume, string parola)
        {
            var iDParameter = iD.HasValue ?
                new ObjectParameter("ID", iD) :
                new ObjectParameter("ID", typeof(int));
    
            var numeParameter = nume != null ?
                new ObjectParameter("nume", nume) :
                new ObjectParameter("nume", typeof(string));
    
            var parolaParameter = parola != null ?
                new ObjectParameter("parola", parola) :
                new ObjectParameter("parola", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("ModifyLogin", iDParameter, numeParameter, parolaParameter);
        }
    
        public virtual int ModifyProfesor(Nullable<int> persID, string nume, Nullable<int> id_login, Nullable<bool> is_diriginte)
        {
            var persIDParameter = persID.HasValue ?
                new ObjectParameter("persID", persID) :
                new ObjectParameter("persID", typeof(int));
    
            var numeParameter = nume != null ?
                new ObjectParameter("nume", nume) :
                new ObjectParameter("nume", typeof(string));
    
            var id_loginParameter = id_login.HasValue ?
                new ObjectParameter("id_login", id_login) :
                new ObjectParameter("id_login", typeof(int));
    
            var is_diriginteParameter = is_diriginte.HasValue ?
                new ObjectParameter("is_diriginte", is_diriginte) :
                new ObjectParameter("is_diriginte", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("ModifyProfesor", persIDParameter, numeParameter, id_loginParameter, is_diriginteParameter);
        }
    
        public virtual ObjectResult<string> SelectMaterie(Nullable<int> iD)
        {
            var iDParameter = iD.HasValue ?
                new ObjectParameter("ID", iD) :
                new ObjectParameter("ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("SelectMaterie", iDParameter);
        }
    
        public virtual ObjectResult<SelectProfesori_Result> SelectProfesori(string name)
        {
            var nameParameter = name != null ?
                new ObjectParameter("name", name) :
                new ObjectParameter("name", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SelectProfesori_Result>("SelectProfesori", nameParameter);
        }
    
        public virtual int sp_alterdiagram(string diagramname, Nullable<int> owner_id, Nullable<int> version, byte[] definition)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            var versionParameter = version.HasValue ?
                new ObjectParameter("version", version) :
                new ObjectParameter("version", typeof(int));
    
            var definitionParameter = definition != null ?
                new ObjectParameter("definition", definition) :
                new ObjectParameter("definition", typeof(byte[]));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_alterdiagram", diagramnameParameter, owner_idParameter, versionParameter, definitionParameter);
        }
    
        public virtual int sp_creatediagram(string diagramname, Nullable<int> owner_id, Nullable<int> version, byte[] definition)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            var versionParameter = version.HasValue ?
                new ObjectParameter("version", version) :
                new ObjectParameter("version", typeof(int));
    
            var definitionParameter = definition != null ?
                new ObjectParameter("definition", definition) :
                new ObjectParameter("definition", typeof(byte[]));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_creatediagram", diagramnameParameter, owner_idParameter, versionParameter, definitionParameter);
        }
    
        public virtual int sp_dropdiagram(string diagramname, Nullable<int> owner_id)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_dropdiagram", diagramnameParameter, owner_idParameter);
        }
    
        public virtual ObjectResult<sp_helpdiagramdefinition_Result> sp_helpdiagramdefinition(string diagramname, Nullable<int> owner_id)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_helpdiagramdefinition_Result>("sp_helpdiagramdefinition", diagramnameParameter, owner_idParameter);
        }
    
        public virtual ObjectResult<sp_helpdiagrams_Result> sp_helpdiagrams(string diagramname, Nullable<int> owner_id)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_helpdiagrams_Result>("sp_helpdiagrams", diagramnameParameter, owner_idParameter);
        }
    
        public virtual int sp_renamediagram(string diagramname, Nullable<int> owner_id, string new_diagramname)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            var new_diagramnameParameter = new_diagramname != null ?
                new ObjectParameter("new_diagramname", new_diagramname) :
                new ObjectParameter("new_diagramname", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_renamediagram", diagramnameParameter, owner_idParameter, new_diagramnameParameter);
        }
    
        public virtual int sp_upgraddiagrams()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_upgraddiagrams");
        }
    
        public virtual int DeleteMaterie(Nullable<int> id)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("id", id) :
                new ObjectParameter("id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteMaterie", idParameter);
        }
    
        public virtual int ModifyMaterie(Nullable<int> iD, string nume, Nullable<int> id_profesor, Nullable<int> id_profil, Nullable<bool> este_teza)
        {
            var iDParameter = iD.HasValue ?
                new ObjectParameter("ID", iD) :
                new ObjectParameter("ID", typeof(int));
    
            var numeParameter = nume != null ?
                new ObjectParameter("nume", nume) :
                new ObjectParameter("nume", typeof(string));
    
            var id_profesorParameter = id_profesor.HasValue ?
                new ObjectParameter("id_profesor", id_profesor) :
                new ObjectParameter("id_profesor", typeof(int));
    
            var id_profilParameter = id_profil.HasValue ?
                new ObjectParameter("id_profil", id_profil) :
                new ObjectParameter("id_profil", typeof(int));
    
            var este_tezaParameter = este_teza.HasValue ?
                new ObjectParameter("este_teza", este_teza) :
                new ObjectParameter("este_teza", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("ModifyMaterie", iDParameter, numeParameter, id_profesorParameter, id_profilParameter, este_tezaParameter);
        }
    }
}
