﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using School_Database_Tema3.Helper;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Models.Actions
{
    class ProfesorActions:BaseVM
    {
        SchoolEntities1 context = new SchoolEntities1();
        private ProfesorVM profesorContext;
        public ProfesorActions(ProfesorVM profilctx)
        {
            this.profesorContext = profilctx;
        }
        public ObservableCollection<ProfesorVM> AllProfesor()
        {
            List<Profesor> profesor = context.Profesors.ToList();
            ObservableCollection<ProfesorVM> result = new ObservableCollection<ProfesorVM>();
            foreach (Profesor prf in profesor)
            {
                result.Add(new ProfesorVM()
                {
                    Id_profesor = prf.id_profesor,
                    Nume = prf.Nume,
                    Id_login=prf.id_login,
                    Is_diriginte=prf.is_diriginte
                });
            }
            return result;
        }
        public void AddMethod(object obj)
        {

            ProfesorVM phoneVM = obj as ProfesorVM;
            if (phoneVM != null)
            {
                if (String.IsNullOrEmpty(phoneVM.Nume))
                    profesorContext.Message = "Numele nu este bun";

                else
                {
                   // context.AddProfesor(phoneVM.Id_profesor, phoneVM.Nume, phoneVM.Id_login, phoneVM.Is_diriginte);
                    context.SaveChanges();
                    profesorContext.ProfesorList = ProfesorsForLogin(phoneVM.Id_login);
                    profesorContext.Message = "";
                }
            }
        }
        public ObservableCollection<ProfesorVM> ProfesorsForLogin(int id_login)
        {
            List<Profesor> phones = context.Profesors.Where(item => item.id_profesor == id_login).ToList();
            ObservableCollection<ProfesorVM> result = new ObservableCollection<ProfesorVM>();
            foreach (Profesor ph in phones)
            {
                result.Add(new ProfesorVM()
                {
                    Id_profesor = ph.id_profesor,
                    Nume = ph.Nume,
                    Is_diriginte = ph.is_diriginte,
                    Id_login = ph.id_login
                });
            }
            return result;
        }
        public void UpdateMethod(object obj)
        {
            
            ProfesorVM loginVM = obj as ProfesorVM;
            if (loginVM == null)
            {
                profesorContext.Message = "Selecteaza un login";
            }
            else
            {
                context.ModifyProfesor(loginVM.Id_profesor, loginVM.Nume, loginVM.Id_login,loginVM.Is_diriginte);
                context.SaveChanges();
                profesorContext.Message = "";
            }
        }

        public void DeleteMethod(object obj)
        {
            
            ProfesorVM loginVM = obj as ProfesorVM;
            if (loginVM == null)
            {
                profesorContext.Message = "Selecteaza un login";
            }
            else
            {
               // Profesor login = context.Profesors.Where(i => i.id_profesor == loginVM.Id_profesor).FirstOrDefault();
                context.DeleteProfesor(loginVM.Id_profesor);
                context.SaveChanges();
                profesorContext.ProfesorList = AllProfesor();
                profesorContext.Message = "";
            }
        }
    }
}
