﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace School_Database_Tema3.Views
{
    /// <summary>
    /// Interaction logic for AdminButoane.xaml
    /// </summary>
    public partial class AdminButoane : Window
    {
        public AdminButoane()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Admin adminWindow1 = new Admin();
            adminWindow1.Show();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ProfesoriAdaugare adminWindow1 = new ProfesoriAdaugare();
            adminWindow1.Show();
        }
    }
}
