﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using School_Database_Tema3.Models.Actions;

namespace School_Database_Tema3.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
           typeBox.Items.Add("Admin");
            typeBox.Items.Add("Professor");
            typeBox.Items.Add("Class master");
            typeBox.Items.Add("Student");
        }
        string Conn = ("Data Source=DESKTOP-6PH36P6;Initial Catalog=School;Integrated Security=true");
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (usernameBox.Text == "" || passwordBox.Text == "" || typeBox.Text == "")
                {
                    MessageBox.Show("Please compleate all fileds!");
                }
                else
                {
                    SqlConnection con = new SqlConnection(Conn);
                    SqlCommand cmd = new SqlCommand("select * from Login where nume_utilizator=@Username and parola=@Password and tip=@Type", con);
                    cmd.Parameters.AddWithValue("@Username", usernameBox.Text);
                    cmd.Parameters.AddWithValue("@Password", passwordBox.Text);
                    cmd.Parameters.AddWithValue("@Type", typeBox.Text);
                    con.Open();
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adpt.Fill(ds);
                    con.Close();
                    int count = ds.Tables[0].Rows.Count;
                    if (count == 1)
                    {
                        if (ds.Tables[0].Rows[0][3].ToString() == "Admin")
                        {
                            AdminButoane adminWindow1 = new AdminButoane();
                            adminWindow1.Show();
                        }
                        else
                        if (ds.Tables[0].Rows[0][3].ToString() == "Student")
                        {
                            ElevWindow adminWindow1 = new ElevWindow();
                            adminWindow1.Show();
                        }


                    }
                    else
                    {
                        MessageBox.Show("Access denied!");
                    }
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
    }
}

