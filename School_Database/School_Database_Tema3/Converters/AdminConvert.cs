﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Converters
{
    class AdminConvert: IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[1] != null)
            {
                return new AdminVM()
                {
                   // Id_admin = values[0].ToString(),
                    Nume = values[1].ToString()
                    
                };
            }
            else
            {
                return null;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            AdminVM pers = value as AdminVM;
            object[] result = new object[1] { pers.Nume};
            return result;
        }
    }
}
