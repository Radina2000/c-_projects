﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Converters
{
    class ElevConvert: IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (values[0] == null)
                values[0] = 1;
            if (values[2] == null)
                values[2] = 1;
            if (values[1] == null)
                values[1] = 1;
            if (values[3] == null)
                values[3] = 1;
            return new ElevVM()
            {
              //Id_elev = int.Parse(values[0].ToString()),
                Nume = values[0].ToString(),
                //Id_login = int.Parse(values[2].ToString()),
                //Id_clasa = int.Parse(values[3].ToString())

            };
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            ElevVM pers = value as ElevVM;
            object[] result = new object[1] {/*pers.Id_elev,*/pers.Nume/*,pers.Id_login ,pers.Id_clasa*/};
            return result;
        }
    }
}
