﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School_Database_Tema3.Helper;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Models.Actions
{
    class CatalogActions:BaseVM
    {
        SchoolEntities1 context = new SchoolEntities1();
        private CatalogVM catalogContext;
        public CatalogActions(CatalogVM catalogContext)
        {
            this.catalogContext = catalogContext;
        }
        public ObservableCollection<CatalogVM> AllCatalog()
        {
            List<Catalog> catalogs = context.Catalogs.ToList();
            ObservableCollection<CatalogVM> result = new ObservableCollection<CatalogVM>();
            foreach (Catalog ctg in catalogs)
            {
                result.Add(new CatalogVM()
                {
                   /* Id_catalog = ctg.id_catalog,
                    Nota = ctg.nota,
                    Absenta = ctg.absenta,
                    Absenta_nemotivata = ctg.absenta_nemotivata,
                    Id_materie=ctg.id_materie,
                    Id_elev=ctg.id_elev*/
                });
            }
            return result;
        }
        public void AddMethod(object obj)
        {
            //parametrul obj este cel dat prin CommandParameter cu MultipleBinding la Button in xaml
            CatalogVM catalogVM = obj as CatalogVM;
            if (catalogVM != null)
            {
                
                    // context.Add(personVM.Name, personVM.Address);
                    //fara a utiliza procedura stocata AddPerson
                    context.Catalogs.Add(new Catalog() { id_catalog = catalogVM.Id_catalog, 
                        nota = catalogVM.Nota, absenta = catalogVM.Absenta, absenta_nemotivata = catalogVM.Absenta,id_materie=catalogVM.Id_materie,
                        id_elev=catalogVM.Id_elev});
                    context.SaveChanges();
                    catalogContext.CatalogList = AllCatalog();
                    catalogContext.Message = "";
                
            }
        }
        public void UpdateMethod(object obj)
        {
            CatalogVM catalogVM = obj as CatalogVM;
            if (catalogVM == null)
            {
                catalogContext.Message = "Selecteaza un catalog";
            }
            else
            {
                context.ModifyCatalog(catalogVM.Id_catalog, catalogVM.Nota, catalogVM.Absenta,
                    catalogVM.Absenta_nemotivata,catalogVM.Id_materie,catalogVM.Id_elev);
                context.SaveChanges();
                catalogContext.Message = "";
            }
        }

        public void DeleteMethod(object obj)
        {
             CatalogVM elevVM = obj as CatalogVM;
            if (elevVM == null)
            {
                catalogContext.Message = "Selecteaza un catalog";
            }
            else
            {
                Catalog catalog = context.Catalogs.Where(i => i.id_catalog == elevVM.Id_catalog).FirstOrDefault();
                context.DeleteElev(elevVM.Id_catalog);
                context.SaveChanges();
                catalogContext.CatalogList = AllCatalog();
                catalogContext.Message = "";
            }
        }
    }
}

