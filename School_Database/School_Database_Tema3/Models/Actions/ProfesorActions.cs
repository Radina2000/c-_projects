﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using School_Database_Tema3.Helper;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Models.Actions
{
    class ProfesorActions:BaseVM
    {
        SchoolEntities1 context = new SchoolEntities1();
        private ProfesorVM profesorContext;
        public ProfesorActions(ProfesorVM profilctx)
        {
            this.profesorContext = profilctx;
        }
        public ObservableCollection<ProfesorVM> AllProfesor()
        {
            List<Profesor> profesor = context.Profesors.ToList();
            ObservableCollection<ProfesorVM> result = new ObservableCollection<ProfesorVM>();
            foreach (Profesor prf in profesor)
            {
                result.Add(new ProfesorVM()
                {
                    Id_profesor = prf.id_profesor,
                    Nume = prf.Nume,
                    Id_login=prf.id_login,
                    Is_diriginte=prf.is_diriginte
                });
            }
            return result;
        }
        public void AddMethod(object obj)
        {
            MessageBox.Show("da");
            ProfesorVM phoneVM = obj as ProfesorVM;

           // context.Profesors.Add(new Profesor() {id_profesor=phoneVM.Id_profesor, Nume = phoneVM.Nume, id_login = phoneVM.Id_login,is_diriginte=phoneVM.Is_diriginte });
            //context.AddProfesor(phoneVM.Id_profesor, phoneVM.Nume, phoneVM.Id_login,phoneVM.Is_diriginte);
              //  context.SaveChanges();
                profesorContext.ProfesorList = AllProfesor();
                profesorContext.Message = "";
            
        }
        public void UpdateMethod(object obj)
        {
            MessageBox.Show("da");
            ProfesorVM loginVM = obj as ProfesorVM;
            if (loginVM == null)
            {
                profesorContext.Message = "Selecteaza un login";
            }
            else
            {
                context.ModifyProfesor(loginVM.Id_profesor, loginVM.Nume, loginVM.Id_login,loginVM.Is_diriginte);
                context.SaveChanges();
                profesorContext.Message = "";
            }
        }

        public void DeleteMethod(object obj)
        {
            MessageBox.Show("da");
            ProfesorVM loginVM = obj as ProfesorVM;
            if (loginVM == null)
            {
                profesorContext.Message = "Selecteaza un login";
            }
            else
            {
               // Profesor login = context.Profesors.Where(i => i.id_profesor == loginVM.Id_profesor).FirstOrDefault();
                context.DeleteProfesor(loginVM.Id_profesor);
                context.SaveChanges();
                profesorContext.ProfesorList = AllProfesor();
                profesorContext.Message = "";
            }
        }
    }
}
