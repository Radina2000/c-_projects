﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School_Database_Tema3.Helper;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Models.Actions
{
    class MaterieActions:BaseVM
    {
        SchoolEntities1 context = new SchoolEntities1();
        private MaterieVM materieContext;
        public MaterieActions(MaterieVM materieContext)
        {
            this.materieContext = materieContext;
        }
        public ObservableCollection<MaterieVM> AllMaterie()
        {
            List<Materie> materie = context.Materies.ToList();
            ObservableCollection<MaterieVM> result = new ObservableCollection<MaterieVM>();
            foreach (Materie lgn in materie)
            {
                result.Add(new MaterieVM()
                {
                    Id_materie = lgn.id_materie,
                    Nume = lgn.Nume,
                    Id_profesor = lgn.id_profesor,
                    Id_profil=lgn.id_profil
                });
            }
            return result;
        }
        /*public void AddMethod(object obj)
        {
            //parametrul obj este cel dat prin CommandParameter cu MultipleBinding la Button in xaml
            LoginVM loginVM = obj as LoginVM;
            if (loginVM != null)
            {
                // context.Add(personVM.Name, personVM.Address);
                //fara a utiliza procedura stocata AddPerson
                context.Logins.Add(new Login()
                {
                    id_login = loginVM.Id_login,
                    nume_utilizator = loginVM.Nume,
                    parola = loginVM.Parola
                });
                context.SaveChanges();
                materieContext.LoginList = AllLogin();
                materieContext.Message = "";
            }
        }
        public void UpdateMethod(object obj)
        {
            LoginVM loginVM = obj as LoginVM;
            if (loginVM == null)
            {
                materieContext.Message = "Selecteaza un login";
            }
            else
            {
                context.ModifyLogin(loginVM.Id_login, loginVM.Nume, loginVM.Parola);
                context.SaveChanges();
                materieContext.Message = "";
            }
        }

        public void DeleteMethod(object obj)
        {
            LoginVM loginVM = obj as LoginVM;
            if (loginVM == null)
            {
                materieContext.Message = "Selecteaza un login";
            }
            else
            {
                Login login = context.Logins.Where(i => i.id_login == loginVM.Id_login).FirstOrDefault();
                //de facut metoda daca am nevoie
                // context.Delete(loginVM.Id_login);
                context.SaveChanges();
                materieContext.LoginList = AllLogin();
                materieContext.Message = "";
            }
        }*/
    }
}
