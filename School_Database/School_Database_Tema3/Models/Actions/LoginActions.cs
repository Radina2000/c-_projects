﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School_Database_Tema3.Helper;
using School_Database_Tema3.ViewModels;

namespace School_Database_Tema3.Models.Actions
{
    class LoginActions: BaseVM
    {
        SchoolEntities1 context = new SchoolEntities1();
        private LoginVM loginContext;
        public LoginActions(LoginVM loginContext)
        {
            this.loginContext = loginContext;
        }
        public ObservableCollection<LoginVM> AllLogin()
        {
            List<Login> logins = context.Logins.ToList();
            ObservableCollection<LoginVM> result = new ObservableCollection<LoginVM>();
            foreach (Login lgn in logins)
            {
                result.Add(new LoginVM()
                {
                    Id_login = lgn.id_login,
                    Nume = lgn.nume_utilizator,
                    Parola = lgn.parola,
                });
            }
            return result;
        }
        public void AddMethod(object obj)
        {
            //parametrul obj este cel dat prin CommandParameter cu MultipleBinding la Button in xaml
            LoginVM loginVM = obj as LoginVM;
            if (loginVM != null)
            {              
                    // context.Add(personVM.Name, personVM.Address);
                    //fara a utiliza procedura stocata AddPerson
                    context.Logins.Add(new Login() { 
                        id_login = loginVM.Id_login, nume_utilizator = loginVM.Nume, parola = loginVM.Parola });
                    context.SaveChanges();
                    loginContext.LoginList = AllLogin();
                    loginContext.Message = "";
            }
        }
        public void UpdateMethod(object obj)
        {
            LoginVM loginVM = obj as LoginVM;
            if (loginVM == null)
            {
                loginContext.Message = "Selecteaza un login";
            }
            else
            {
                context.ModifyLogin(loginVM.Id_login, loginVM.Nume, loginVM.Parola);
                context.SaveChanges();
                loginContext.Message = "";
            }
        }

        public void DeleteMethod(object obj)
        {
            LoginVM loginVM = obj as LoginVM;
            if (loginVM == null)
            {
                loginContext.Message = "Selecteaza un login";
            }
            else
            {
                Login login = context.Logins.Where(i => i.id_login == loginVM.Id_login).FirstOrDefault();
                //de facut metoda daca am nevoie
               // context.Delete(loginVM.Id_login);
                context.SaveChanges();
                loginContext.LoginList = AllLogin();
                loginContext.Message = "";
            }
        }
    }
}

