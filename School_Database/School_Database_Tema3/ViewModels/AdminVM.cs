﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using School_Database_Tema3.Helper;
using School_Database_Tema3.Models.Actions;

namespace School_Database_Tema3.ViewModels
{
    class AdminVM:BaseVM
    {
        AdminActions aAct;
        //LoginActions lAct;

        public AdminVM()
        {
            aAct = new AdminActions(this);
            //lAct = new LoginActions(this);
        }

        #region Data Members

        private int id_admin;
        private string nume;
        private int id_login;
        private ObservableCollection<AdminVM> adminList;
       // private ObservableCollection<LoginVM> loginList;

        public int Id_admin
        {
            get
            {
                return id_admin;
            }
            set
            {
                id_admin = value;
                NotifyPropertyChanged("id_admin");
            }
        }

        public string Nume
        {
            get
            {
                return nume;
            }
            set
            {
                nume = value;
                NotifyPropertyChanged("nume");
            }
        }

        public int Id_login
        {
            get
            {
                return id_login;
            }
            set
            {
                id_login = value;
                NotifyPropertyChanged("id_login");
            }
        }

        public ObservableCollection<AdminVM> AdminList
        {
            get
            {
                adminList = aAct.AllAdmins();
                return adminList;
            }
            set
            {
                adminList = value;
                NotifyPropertyChanged("PersonsList");
            }
        }

        #endregion

        #region Command Members

        //asta este pt butonul Add
        private ICommand addCommand;
        public ICommand AddCommandAdmin
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new RelayCommand(aAct.AddMethod);
                }
                return addCommand;
            }
        }

        //asta este pt butonul Update
        private ICommand updateCommand;
        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new RelayCommand(aAct.UpdateMethod);
                }
                return updateCommand;
            }
        }
        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new RelayCommand(aAct.DeleteMethod);
                }
                return deleteCommand;
            }
        }
        public string Message { get; internal set; }

        #endregion
    }
}

