﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using School_Database_Tema3.Helper;
using School_Database_Tema3.Models.Actions;

namespace School_Database_Tema3.ViewModels
{
    class MaterieVM:BaseVM
    {
        MaterieActions mAct;

        public MaterieVM()
        {
            mAct = new MaterieActions(this);

        }

        #region Data Members

        private int id_materie;
        private string nume;
        private int id_profesor;
        private int id_profil;
        private bool este_teza;
        private string message;
        private ObservableCollection<MaterieVM> materieList;
        public int Id_materie
        {
            get
            {
                return id_materie;
            }
            set
            {
                id_materie = value;
                NotifyPropertyChanged("id_materie");
            }
        }

        public string Nume
        {
            get
            {
                return nume;
            }
            set
            {
                nume = value;
                NotifyPropertyChanged("nume");
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }
        public ObservableCollection<MaterieVM> LoginList
        {
            get
            {
                materieList = mAct.AllMaterie();
                return materieList;
            }
            set
            {
                materieList = value;
                NotifyPropertyChanged("LoginList");
            }
        }
        public int Id_profesor
        {
            get
            {
                return id_profesor;
            }
            set
            {
                id_profesor = value;
                NotifyPropertyChanged("id_profesor");
            }
        }

        public int Id_profil
        {
            get
            {
                return id_profil;
            }
            set
            {
                id_profil = value;
                NotifyPropertyChanged("id_profil");
            }
        }
        public bool Este_teza
        {
            get
            {
                return este_teza;
            }
            set
            {
                este_teza = value;
                NotifyPropertyChanged("este_teza");
            }
        }

       /* #endregion
        #region Command Members

        //asta este pt butonul Add
        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new RelayCommand(mAct.AddMethod);
                }
                return addCommand;
            }
        }

        //asta este pt butonul Update
        private ICommand updateCommand;
        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new RelayCommand(mAct.UpdateMethod);
                }
                return updateCommand;
            }
        }

        //asta este pt butonul Delete
        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new RelayCommand(mAct.DeleteMethod);
                }
                return deleteCommand;
            }
        }*/

        #endregion
    }
}
