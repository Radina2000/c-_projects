﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School_Database_Tema3.Helper;
using School_Database_Tema3.Models.Actions;

namespace School_Database_Tema3.ViewModels
{
    class ElevSelectVM:BaseVM
    {
      
        ElevSelectActions eAct;
        public ObservableCollection<ElevSelect_Result> elevSelectListFinal { get; set; }
       // LoginActions lAct;
        // ClasaActions cAct;

        public ElevSelectVM()
        {
            eAct = new ElevSelectActions(this);
            elevSelectListFinal = eAct.AllElevSelect();
            //lAct = new LoginActions(null);
            //cAct = new ClasaActions(this);
        }

        #region Data Members
        private string nume;
        private Nullable<float> nota;
        private Nullable<System.DateTime> absenta;
        private Nullable<System.DateTime> absenta_nemotivata;
        private string message;
        private ObservableCollection<ElevSelect_Result> elevSelectList;
       // private ObservableCollection<LoginVM> loginList;
        // private ObservableCollection<ClasaVM> clasaList;
    

        public string Nume
        {
            get
            {
                return nume;
            }
            set
            {
                nume = value;
                NotifyPropertyChanged("nume");
            }
        }

        public Nullable<float> Nota
        {
            get
            {
                return nota;
            }
            set
            {
                nota = value;
                NotifyPropertyChanged("nota");
            }
        }

        public Nullable<System.DateTime> Absenta
        {
            get
            {
                return absenta;
            }
            set
            {
                absenta = value;
                NotifyPropertyChanged("absenta");
            }
        }

        public Nullable<System.DateTime> Absenta_nemotivata
        {
            get
            {
                return absenta_nemotivata;
            }
            set
            {
                absenta_nemotivata = value;
                NotifyPropertyChanged("absenta_nemotivata");
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }
        public ObservableCollection<ElevSelect_Result> ElevList
        {
            get
            {
                elevSelectList = eAct.AllElevSelect();
                return elevSelectList;
            }
            set
            {
                elevSelectList = value;
                NotifyPropertyChanged("ElevSelectList");
            }
        }
      

       /* #endregion
        #region Command Members

        //asta este pt butonul Add
        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new RelayCommand(eAct.AddMethod);
                }
                return addCommand;
            }
        }

        //asta este pt butonul Update
        private ICommand updateCommand;
        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new RelayCommand(eAct.UpdateMethod);
                }
                return updateCommand;
            }
        }

        //asta este pt butonul Delete
        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new RelayCommand(eAct.DeleteMethod);
                }
                return deleteCommand;
            }
        }

        private ICommand elevSelectCommand;
        public ICommand ElevSelectCommand
        {
            get
            {
                if (elevSelectCommand == null)
                {
                    elevSelectCommand = new RelayCommand(eAct.Elevi_Select);
                }
                return elevSelectCommand;
            }
        }
       */
        #endregion
    }
}

