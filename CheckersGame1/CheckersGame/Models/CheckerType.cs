﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckersGame.Models
{
    public enum CheckerType
    {
        Free, // no value
        white_check, // player ones checker
        white_king, // player ones king 
        black_check, // player twos checker
        black_king // player twos king
    }
}
