﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Views.CheckersGame.Models
{
    public class StatisticsM : INotifyPropertyChanged
    {
        private String totalGames;
        private String redWinnings;
        private String whiteWinnings;
        private String games;
        private String red;
        private String white;

        public String TotalGames
        {
            get
            {
                return totalGames;
            }
            set
            {
                totalGames = value;
                OnPropertyChanged("TotalGames");
            }
        }
        public String RedWinnings
        {
            get
            {
                return redWinnings;
            }
            set
            {
                redWinnings = value;
                OnPropertyChanged("RedWinnings");
            }
        }
        public String WhiteWinnings
        {
            get
            {
                return whiteWinnings;
            }
            set
            {
                whiteWinnings = value;
                OnPropertyChanged("WhiteWinnings");
            }
        }

        public String Games
        {
            get
            {
                return games;
            }
            set
            {
                games = value;
                OnPropertyChanged("Games");
            }
        }
        public String Red
        {
            get
            {
                return red;
            }
            set
            {
                red = value;
                OnPropertyChanged("Red");
            }
        }
        public String White
        {
            get
            {
                return white;
            }
            set
            {
                white = value;
                OnPropertyChanged("White");
            }
        }
        public override string ToString()
        {
            return $"{TotalGames}: {Games}\n {RedWinnings}: {Red}\n {WhiteWinnings}: {White}";
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }


}
