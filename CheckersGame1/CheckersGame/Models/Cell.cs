﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CheckersGame.Models
{
    public class Cell: INotifyPropertyChanged
    {
        public Cell() { }
        public Cell(int x, int y, string displayed,CheckerType checkerType)
        {
            this._X = x;
            this._Y = y;
            this.DisplayedImage = displayed;
            this.Checker_Type = checkerType;
        }
        public Cell(string displayed)
        {
            this.displayedImage = displayed;
        }
        private CheckerType checkerType;
        public CheckerType Checker_Type
        {
            get { return checkerType; }
            set
            {
                checkerType = value;
                NotifyPropertyChanged("CheckerType");
            }
        }
        private int _x;
        public int _X
        {
            get { return _x; }
            set
            {
                _x = value;
                NotifyPropertyChanged("X");
            }
        }
        private int _y;
        public int _Y
        {
            get { return _y; }
            set
            {
                _y = value;
                NotifyPropertyChanged("Y");
            }
        }
        private string displayedImage;
        public string DisplayedImage
        {
            get { return displayedImage; }
            set
            {
                displayedImage = value;
                NotifyPropertyChanged("DisplayedImage");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
