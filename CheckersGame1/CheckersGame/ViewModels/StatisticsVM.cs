﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Views.CheckersGame.Models;

namespace CheckersGame.ViewModels
{
    public class StatisticsVM
    {
        public ObservableCollection<StatisticsM> Statistics { get; set; }
        private String totalGames;
        private String redWinnings;
        private String whiteWinnings;
        private String games;
        private String red;
        private String white;

        public StatisticsVM()
        {
            Reading();
            Statistics = new ObservableCollection<StatisticsM>
            {
                new StatisticsM { TotalGames = totalGames, RedWinnings = redWinnings, WhiteWinnings = whiteWinnings, Games=games, Red=red, White=white }
            };
        }
        public void Reading()
        {
            try
            {
                StreamReader streamReader = new StreamReader(@"C:\Users\Laptop\Desktop\Statistics.txt");
                
                String str = streamReader.ReadLine();
                totalGames = str;

                str = streamReader.ReadLine();
                redWinnings = str;

                str = streamReader.ReadLine();
                whiteWinnings = str;

                str = streamReader.ReadLine();
                games = str;

                str = streamReader.ReadLine();
                red = str;

                str = streamReader.ReadLine();
                white = str;

                streamReader.Close();
            }
            catch(Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
        }

        
    }
}
