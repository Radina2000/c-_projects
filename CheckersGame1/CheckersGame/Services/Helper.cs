﻿using CheckersGame.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckersGame.Services
{
    class Helper
    {
        public static Cell CurrentCell { get; set; }
        public static Cell PreviousCell { get; set; }
        public static Cell MovedCell { get; set; }
        public static ObservableCollection<ObservableCollection<Cell>> InitGameBoard()
        {
            ObservableCollection<ObservableCollection<Cell>> board = new ObservableCollection<ObservableCollection<Cell>>();

            for (int i = 0; i < 8; i++)
            {
                ObservableCollection<Cell> line = new ObservableCollection<Cell>();
                for (int j = 0; j < 8; j++)
                {
                    Cell boardCell = new Cell();

                    if ((i + j) % 2 == 0)
                    {
                        if (i < 3)
                        {
                            boardCell.DisplayedImage = @"C:\Users\Radina\Desktop\ANUL 2\MVP\gameLavi\CheckersGame\Resources\white.png";
                            boardCell.Checker_Type = CheckerType.white_check;
                        }
                        else
                        if (i > 4)
                        {
                            boardCell.DisplayedImage = @"C:\Users\Radina\Desktop\ANUL 2\MVP\gameLavi\CheckersGame\Resources/black.png";
                            boardCell.Checker_Type = CheckerType.black_check;
                        }
                        else
                        {
                            boardCell.DisplayedImage = @"C:\Users\Radina\Desktop\ANUL 2\MVP\gameLavi\CheckersGame\Resources/dark.png";
                            boardCell.Checker_Type = CheckerType.Free;
                        }
                    }
                    else
                    {
                        boardCell.DisplayedImage = @"C:\Users\Radina\Desktop\ANUL 2\MVP\gameLavi\CheckersGame\Resources/light.png";
                        boardCell.Checker_Type = CheckerType.Free;
                    }

                    line.Add(boardCell);
                }
                board.Add(line);
            }

            return board;
        }
    }
}
