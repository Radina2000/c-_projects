﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using CheckersGame.Models;

namespace CheckersGame.Services
{
    class GameBusinessLogic
    {
        private ObservableCollection<ObservableCollection<Cell>> cells;
        public GameBusinessLogic(ObservableCollection<ObservableCollection<Cell>> cells)
        {
            this.cells = cells;
        }

        private void TurnCard(Cell cell)
        {
            cell.DisplayedImage = cell.DisplayedImage;
        }

        private void TurnCardBack(Cell cell)
        {
            for (int i = 0; i < 8; i++)
            {               
                for (int j = 0; j < 8; j++)
                {
                    if ((i+j)%2==0)
                        cell.DisplayedImage = @"C:\Users\Radina\Desktop\ANUL 2\MVP\gameLavi\CheckersGame\Resources\dark.png";
                    else
                       cell.DisplayedImage = @"C:\Users\Radina\Desktop\ANUL 2\MVP\gameLavi\CheckersGame\Resources\light.png";

                }       
            }
        }
      
        public void MovePieceToNewPlace(Cell currentCell)
        {
            Helper.CurrentCell = currentCell;
            Cell moved = new Cell();
            int x = currentCell._X;
            int y = currentCell._Y;
            moved._X = x - 1;
            moved._Y = y - 1;
            moved.DisplayedImage = @"C:\Users\Radina\Desktop\ANUL 2\MVP\gameLavi\CheckersGame\Resources\black.png";
            moved.Checker_Type = CheckerType.black_check;
            TurnCard(moved);
        }
        
        public void Move(Cell currentCell)
        {
            Helper.CurrentCell = currentCell;
            TurnCard(currentCell);
            if (Helper.PreviousCell != null)
            {
                TurnCardBack(Helper.PreviousCell);
            }
            Helper.PreviousCell = currentCell;
        }
        public void ClickAction(Cell obj)
        {
            Move(obj);
            MovePieceToNewPlace(obj);
        }
    }
}
