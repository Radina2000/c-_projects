﻿using CheckersGame.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckersGame.Services
{
    class Helper
    {
        public static Cell CurrentCell { get; set; }
        public static Cell PreviousCell { get; set; }
        public static Cell MovedCell { get; set; }
        public static ObservableCollection<ObservableCollection<Cell>> InitGameBoard()
        {
            ObservableCollection<ObservableCollection<Cell>> board = new ObservableCollection<ObservableCollection<Cell>>();

            for (int i = 0; i < 8; i++)
            {
                ObservableCollection<Cell> line = new ObservableCollection<Cell>();
                for (int j = 0; j < 8; j++)
                {
                    Cell boardCell = new Cell();

                    if ((i + j) % 2 != 0)
                    {
                        if (i < 3)
                        {
                           boardCell.DisplayedImage = @"C:\Users\Radina\Desktop\ANUL 2\MVP\Abraham_Radina_Checkers\CheckersGame1\Resources\white.png";
                           boardCell.Checker_Type = "white";
                            boardCell.X = i;
                            boardCell.Y = j;
                        }
                        else
                        if (i > 4)
                        {
                           boardCell.DisplayedImage = @"C:\Users\Radina\Desktop\ANUL 2\MVP\Abraham_Radina_Checkers\CheckersGame1\Resources\black.png";
                            boardCell.Checker_Type ="black";
                            boardCell.X = i;
                            boardCell.Y = j;
                        }
                        else
                        {
                           boardCell.DisplayedImage = @"C:\Users\Radina\Desktop\ANUL 2\MVP\Abraham_Radina_Checkers\CheckersGame1\Resources\dark.png";
                            boardCell.Checker_Type = "dark";
                            boardCell.X = i;
                            boardCell.Y = j;

                        }
                    }
                    else
                    {
                        boardCell.DisplayedImage = @"C:\Users\Radina\Desktop\ANUL 2\MVP\Abraham_Radina_Checkers\CheckersGame1\Resources\light.png";
                        boardCell.Checker_Type = "light";
                        boardCell.X = i;
                        boardCell.Y = j;
                    }

                    line.Add(boardCell);
                }
                board.Add(line);
            }

            return board;
        }
    }
}
