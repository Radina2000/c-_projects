﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using CheckersGame.Models;

namespace CheckersGame.Services
{
    class GameBusinessLogic
    {
        private Cell currentCell;
        private Cell previousCell;
        public int capturatedWhitePieces = 12;
        public int capturatedBlackPieces = 12;
        private bool firstClick = true;
        private bool first = true;
        private bool second = false;
        //StreamWriter fout = new StreamWriter(@"C:\Users\Radina\Desktop\ANUL 2\MVP\Abraham_Radina_Checkers\CheckersGame1\Resources\checkers.txt");
        private ObservableCollection<ObservableCollection<Cell>> cells;

        public GameBusinessLogic(ObservableCollection<ObservableCollection<Cell>> cells)
        {
            this.cells = cells;
        }
        public void ChangePieces()
        {
            Cell Aux = new Cell();
            Aux.DisplayedImage = currentCell.DisplayedImage;
            Aux.Checker_Type = currentCell.Checker_Type;

            cells[currentCell.X][currentCell.Y].DisplayedImage = previousCell.DisplayedImage;
            cells[currentCell.X][currentCell.Y].Checker_Type = previousCell.Checker_Type;
            cells[previousCell.X][previousCell.Y].DisplayedImage = Aux.DisplayedImage;
            cells[previousCell.X][previousCell.Y].Checker_Type = Aux.Checker_Type;

        }
        public bool Black_King_Capture()
        {
            if (currentCell.X + 2 != previousCell.X && currentCell.X - 2 != previousCell.X)
            {
                return false;
            }
            if (currentCell.Y + 2 == previousCell.Y &&
                 (cells[previousCell.X + 1][previousCell.Y - 1].Checker_Type == "white" ||
                 cells[previousCell.X + 1][previousCell.Y - 1].Checker_Type == "king_white"))
            {
                cells[previousCell.X + 1][previousCell.Y - 1].Checker_Type = "dark";
                cells[previousCell.X + 1][previousCell.Y - 1].DisplayedImage =
                   @"..\Resources\dark.png";
                capturatedWhitePieces--;
                return true;
            }
            if (currentCell.Y + 2 == previousCell.Y &&
                 (cells[previousCell.X - 1][previousCell.Y - 1].Checker_Type == "white" ||
                  cells[previousCell.X - 1][previousCell.Y - 1].Checker_Type == "king_white"))
            {
                cells[previousCell.X - 1][previousCell.Y - 1].Checker_Type = "dark";
                cells[previousCell.X - 1][previousCell.Y - 1].DisplayedImage =
                   @"..\Resources\dark.png";
                capturatedWhitePieces--;
                return true;
            }

            if (currentCell.Y - 2 == previousCell.Y &&
                 (cells[previousCell.X + 1][previousCell.Y + 1].Checker_Type == "white" ||
                  cells[previousCell.X + 1][previousCell.Y + 1].Checker_Type == "king_white"))
            {
                cells[previousCell.X + 1][previousCell.Y + 1].Checker_Type = "dark";
                cells[previousCell.X + 1][previousCell.Y + 1].DisplayedImage =
                    @"..\Resources\dark.png";
                capturatedWhitePieces--;
                return true;
            }

            if (currentCell.Y - 2 == previousCell.Y &&
                (cells[previousCell.X - 1][previousCell.Y + 1].Checker_Type == "white" ||
                 cells[previousCell.X - 1][previousCell.Y + 1].Checker_Type == "king_white"))
            {
                cells[previousCell.X - 1][previousCell.Y + 1].Checker_Type = "dark";
                cells[previousCell.X - 1][previousCell.Y + 1].DisplayedImage =
                    @"..\Resources\dark.png";
                capturatedWhitePieces--;
                return true;
            }
            return false;
        }

        public bool White_King_Capture()
        {
            if (currentCell.X + 2 != previousCell.X && currentCell.X - 2 != previousCell.X)
            {
                return false;
            }
            if (currentCell.Y + 2 == previousCell.Y &&
                 (cells[previousCell.X + 1][previousCell.Y - 1].Checker_Type == "black" ||
                 cells[previousCell.X + 1][previousCell.Y - 1].Checker_Type == "king_black"))
            {
                cells[previousCell.X + 1][previousCell.Y - 1].Checker_Type = "dark";
                cells[previousCell.X + 1][previousCell.Y - 1].DisplayedImage =
                   @"..\Resources\dark.png";
                capturatedBlackPieces--;
                return true;
            }
            if (currentCell.Y + 2 == previousCell.Y &&
                 (cells[previousCell.X - 1][previousCell.Y - 1].Checker_Type == "black" ||
                  cells[previousCell.X - 1][previousCell.Y - 1].Checker_Type == "king_black"))
            {
                cells[previousCell.X - 1][previousCell.Y - 1].Checker_Type = "dark";
                cells[previousCell.X - 1][previousCell.Y - 1].DisplayedImage =
                   @"..\Resources\dark.png";
                capturatedBlackPieces--;
                return true;
            }

            if (currentCell.Y - 2 == previousCell.Y &&
                 (cells[previousCell.X + 1][previousCell.Y + 1].Checker_Type == "black" ||
                  cells[previousCell.X + 1][previousCell.Y + 1].Checker_Type == "king_black"))
            {
                cells[previousCell.X + 1][previousCell.Y + 1].Checker_Type = "dark";
                cells[previousCell.X + 1][previousCell.Y + 1].DisplayedImage =
                    @"..\Resources\dark.png";
                capturatedBlackPieces--;
                return true;
            }

            if (currentCell.Y - 2 == previousCell.Y &&
                (cells[previousCell.X - 1][previousCell.Y + 1].Checker_Type == "black" ||
                 cells[previousCell.X - 1][previousCell.Y + 1].Checker_Type == "king_black"))
            {
                cells[previousCell.X - 1][previousCell.Y + 1].Checker_Type = "dark";
                cells[previousCell.X - 1][previousCell.Y + 1].DisplayedImage =
                    @"..\Resources\dark.png";
                capturatedBlackPieces--;
                return true;
            }
            return false;
        }
        public bool simpleCapture()
        {
            if (currentCell.Checker_Type != "dark")
                return false;

            if (previousCell.Checker_Type == "black")
            {
                if (currentCell.X + 2 != previousCell.X)
                {
                    return false;
                }
                if (currentCell.Y - 2 == previousCell.Y &&
                    (cells[previousCell.X - 1][previousCell.Y + 1].Checker_Type == "white" ||
                     cells[previousCell.X - 1][previousCell.Y + 1].Checker_Type == "king_white"))
                {
                    cells[previousCell.X - 1][previousCell.Y + 1].Checker_Type = "dark";
                    cells[previousCell.X - 1][previousCell.Y + 1].DisplayedImage =
                       @"..\Resources\dark.png";
                    capturatedWhitePieces--;
                    return true;
                }

                if (currentCell.Y + 2 == previousCell.Y &&
                     (cells[previousCell.X - 1][previousCell.Y - 1].Checker_Type == "white" ||
                     cells[previousCell.X - 1][previousCell.Y - 1].Checker_Type == "king_white"))
                {
                    cells[previousCell.X - 1][previousCell.Y - 1].Checker_Type = "dark";
                    cells[previousCell.X - 1][previousCell.Y - 1].DisplayedImage =
                        @"..\Resources\dark.png";
                    capturatedWhitePieces--;
                    return true;
                }
            }
            if (previousCell.Checker_Type == "king_black")
            {
                return Black_King_Capture();
            }
            if (previousCell.Checker_Type == "white")
            {
                if (currentCell.X - 2 != previousCell.X)
                {
                    return false;
                }
                if (currentCell.Y + 2 == previousCell.Y
                    && (cells[previousCell.X + 1][previousCell.Y - 1].Checker_Type == "black" ||
                    cells[previousCell.X + 1][previousCell.Y - 1].Checker_Type == "king_black"))
                {
                    cells[previousCell.X + 1][previousCell.Y - 1].Checker_Type = "dark";
                    cells[previousCell.X + 1][previousCell.Y - 1].DisplayedImage =
                        @"..\Resources\dark.png";
                    capturatedBlackPieces--;
                    return true;
                }
                if (currentCell.Y - 2 == previousCell.Y
                    && (cells[previousCell.X + 1][previousCell.Y + 1].Checker_Type == "black" ||
                     cells[previousCell.X + 1][previousCell.Y + 1].Checker_Type == "king_black"))
                {
                    cells[previousCell.X + 1][previousCell.Y + 1].Checker_Type = "dark";
                    cells[previousCell.X + 1][previousCell.Y + 1].DisplayedImage =
                        @"..\Resources\dark.png";
                    capturatedBlackPieces--;
                    return true;
                }
            }

            if (previousCell.Checker_Type == "king_white")
            {
                return White_King_Capture();
            }

            return false;
        }
        public bool SimpleMoveValidation()
        {
            if (currentCell.Checker_Type != "dark")
                return false;

            if (previousCell.Checker_Type == "black")
            {
                if (currentCell.X + 1 != previousCell.X)
                    return false;
                if (currentCell.Y - 1 != previousCell.Y && currentCell.Y + 1 != previousCell.Y)
                    return false;
            }

            if (previousCell.Checker_Type == "white")
            {
                if (currentCell.X - 1 != previousCell.X)
                    return false;
                if (currentCell.Y - 1 != previousCell.Y && currentCell.Y + 1 != previousCell.Y)
                    return false;
            }
            if (previousCell.Checker_Type == "king_black" || previousCell.Checker_Type == "king_white")
            {
                if (currentCell.X - 1 != previousCell.X && currentCell.X + 1 != previousCell.X)
                    return false;
                if (currentCell.Y - 1 != previousCell.Y && currentCell.Y + 1 != previousCell.Y)
                    return false;
            }
            return true;

        }
        public void Change_king()
        {
            if (currentCell.Checker_Type == "black")
            {
                if (currentCell.X == 0)
                {
                    currentCell.DisplayedImage = @"C:\Users\Radina\Desktop\ANUL 2\MVP\Abraham_Radina_Checkers\CheckersGame1\CheckersGame\Resources\king_black.png";
                    currentCell.Checker_Type = "king_black";
                }
            }

            if (currentCell.Checker_Type == "white")
            {
                if (currentCell.X == 7)
                {
                    currentCell.DisplayedImage = @"C:\Users\Radina\Desktop\ANUL 2\MVP\Abraham_Radina_Checkers\CheckersGame1\CheckersGame\Resources\king_white.png";
                    currentCell.Checker_Type = "king_white";
                }
            }
        }
        public void Game(Cell obj)
        {
            currentCell = obj;
            if (firstClick == true)
            {
                if ((currentCell.Checker_Type == "black" || currentCell.Checker_Type == "king_black") && first == true)
                {
                    previousCell = currentCell;
                    firstClick = false;
                    first = false;
                    second = true;

                }
                if ((currentCell.Checker_Type == "white" || currentCell.Checker_Type == "king_white") && second == true)
                {
                    previousCell = currentCell;
                    firstClick = false;
                    first = true;
                    second = false;
                }
            }
            else
            {
                if (SimpleMoveValidation() == true)
                {
                    ChangePieces();
                    firstClick = true;
                }
                else
                {
                    
                    if (simpleCapture() == true)
                    {
                        ChangePieces();
                        if (capturatedBlackPieces == 0)
                        {
                            //fout.WriteLine("WHITE PLAYER WINS");
                            MessageBox.Show("WHITE PLAYER WINS");
                            //fout.Close();
                        }
                        if (capturatedWhitePieces == 0)
                        {
                            //fout.WriteLine("BLACK PLAYER WINS");
                            MessageBox.Show("BLACK PLAYER WINS");
                           // fout.Close();
                        }
                        firstClick = true;
                    }
                    else
                    {
                        firstClick = true;
                        if (first == true)
                        {
                            first = false;
                            second = true;
                        }
                        else
                        {
                            first = true;
                            second = false;
                        }
                    }

                }
                Change_king();
            }
        }
        public void ClickAction(Cell obj)
        {
            Game(obj);
        }
    }
}
    
