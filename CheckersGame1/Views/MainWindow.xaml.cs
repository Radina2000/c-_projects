﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheckersGame.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void AboutGameButton_Click(object sender, RoutedEventArgs e)
        {
            ProcessStartInfo showFile = new ProcessStartInfo();
            showFile.FileName = "notepad.exe";
            showFile.Arguments = @"C:\Users\Radina\Desktop\ANUL 2\MVP\Abraham_Radina_Checkers\CheckersGame1\Resources\about.txt";
            Process.Start(showFile);
        }

        private void NewGameButton_Click(object sender, RoutedEventArgs e)
        {
            NewGame newGameWindow = new NewGame();
            newGameWindow.Show();
        }

        private void StatisticsButton_Click(object sender, RoutedEventArgs e)
        {
            ProcessStartInfo showFile = new ProcessStartInfo();
            showFile.FileName = "notepad.exe";
            showFile.Arguments = @"C:\Users\Radina\Desktop\ANUL 2\MVP\Abraham_Radina_Checkers\CheckersGame1\Resources\checkers.txt";
            Process.Start(showFile);
        }
    }
}
